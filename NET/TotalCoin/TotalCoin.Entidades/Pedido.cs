﻿using TotalCoin.Entidades.Enums;

namespace TotalCoin.Entidades
{
    public class Pedido : EntidadBase
    {
        #region Constructores

        public Pedido(Producto producto, int cantidad, Estado estado)
        {
            ProductId = producto.Id;
            Amount = cantidad;
            Price = cantidad * producto.Price;
            Status = (int)estado;
        }

        public Pedido()
        {
        }

        #endregion Constructores

        public virtual Producto Product { get; set; }

        public int ProductId { get; set; }

        public int Amount { get; set; }

        public decimal Price { get; set; }

        public int Status { get; set; }
    }
}