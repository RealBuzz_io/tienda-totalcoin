﻿using System.Collections.Generic;

namespace TotalCoin.Entidades
{
    public class Producto : EntidadBase
    {
        #region Constructores

        public Producto()
        {
        }

        public Producto(string nombre, string descripcion, Categoria categoria, Categoria subcategoria, string codigo, decimal precio, int stock, IList<Imagen> imagenes, IList<FormaDePago> formasDePago, int tiendaId)
        {
            Name = nombre;
            Description = descripcion;
            Code = codigo;
            Stock = stock;
            Images = imagenes;
            PaymentMethods = formasDePago;
            Category = categoria;
            Subcategory = subcategoria;
            Price = precio;
            StoreId = tiendaId;
        }

        public Producto(string nombre, Categoria subcategoria, Categoria categoria, decimal precio)
        {
            Name = nombre;
            Category = categoria;
            Subcategory = subcategoria;
            Price = precio;
        }

        #endregion Constructores

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual Categoria Category { get; set; }

        public virtual Categoria Subcategory { get; set; }

        public decimal Price { get; set; }

        public Tienda Store { get; set; }

        public int? StoreId { get; set; }

        public int Stock { get; set; }

        public string Code { get; set; }

        public virtual IList<FormaDePago> PaymentMethods { get; set; }

        public virtual IList<Imagen> Images { get; set; }

        public bool Deleted { get; set; }
    }
}