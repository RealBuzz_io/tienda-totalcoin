﻿using System;

namespace TotalCoin.Entidades
{
    public class Tienda : EntidadBase
    {
        #region Constructores

        public Tienda()
        {
        }

        public Tienda(int id, string nombre, string imagen, string email, string appKey, Guid merchantId)
        {
            Id = id;
            Name = nombre;
            Image = imagen;
            Email = email;
            AppKey = appKey;
            MerchantId = merchantId;
        }

        public Tienda(string nombre, string imagen, string email, string appKey, Guid merchantId)
        {
            Name = nombre;
            Image = imagen;
            Email = email;
            AppKey = appKey;
            MerchantId = merchantId;
        }

        #endregion Constructores

        public string Name { get; set; }

        public string Image { get; set; } //Ruta relativa

        public string AppKey { get; set; }

        public string Email { get; set; }

        public Guid MerchantId { get; set; }
    }
}