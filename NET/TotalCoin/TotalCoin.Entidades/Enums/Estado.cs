﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotalCoin.Entidades.Enums
{
    public enum Estado
    {
        Pendiente = 1,
        Cancelado = 2,
        Confirmado = 3
    }
}
