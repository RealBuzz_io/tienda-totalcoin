﻿using TotalCoin.Entidades.Facebook;

namespace TotalCoin.Entidades
{
    public class FanpageTienda
    {
        #region Constructores

        public FanpageTienda()
        {
        }

        public FanpageTienda(Fanpage fanpage, Tienda tienda)
        {
            Id = fanpage.Id;
            Store = tienda;
        }

        #endregion Constructores

        public string Id { get; set; }

        public Tienda Store { get; set; }
    }
}