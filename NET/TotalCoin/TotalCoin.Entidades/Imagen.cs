﻿namespace TotalCoin.Entidades
{
    public class Imagen : EntidadBase
    {
        #region Constructores

        public Imagen()
        {
        }

        public Imagen(string ruta, int orden)
        {
            Path = ruta;
            Order = orden;
        }

        #endregion Constructores

        public string Path { get; set; }

        public int Order { get; set; }
    }
}