﻿using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class FacebookTab
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "application")]
        public FacebookApplication Application { get; set; }
    }
}