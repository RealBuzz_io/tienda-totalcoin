﻿using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class Fanpage
    {
        #region Constructores

        public Fanpage()
        {
        }

        public Fanpage(string id, string nombre, bool integrada, string pageToken)
        {
            Id = id;
            Nombre = nombre;
            Integrada = integrada;
            PageToken = pageToken;
        }

        #endregion Constructores

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Nombre { get; set; }

        [DataMember(Name = "access_token")]
        public string PageToken { get; set; }

        public bool Integrada { get; set; }
    }
}