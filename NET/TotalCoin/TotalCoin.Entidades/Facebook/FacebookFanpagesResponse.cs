﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class FacebookFanpagesResponse : FacebookResponse
    {
        [DataMember(Name = "data")]
        public IList<Fanpage> Fanpages { get; set; }
    }
}