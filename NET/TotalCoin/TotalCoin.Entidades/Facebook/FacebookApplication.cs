﻿using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class FacebookApplication
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}