﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class FacebookTabsResponse : FacebookResponse
    {
        [DataMember(Name = "data")]
        public IList<FacebookTab> Tabs { get; set; }
    }
}