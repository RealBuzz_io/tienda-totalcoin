﻿using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class FacebookPaging
    {
        [DataMember(Name = "cursors")]
        public Cursor Cursor { get; set; }

        [DataMember(Name = "previous")]
        public string Previous { get; set; }

        [DataMember(Name = "next")]
        public string Next { get; set; }
    }

    [DataContract]
    public class Cursor
    {
        [DataMember(Name = "before")]
        public string Before { get; set; }

        [DataMember(Name = "after")]
        public string After { get; set; }
    }
}