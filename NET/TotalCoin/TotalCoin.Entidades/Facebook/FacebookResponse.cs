﻿using System.Runtime.Serialization;

namespace TotalCoin.Entidades.Facebook
{
    [DataContract]
    public class FacebookResponse
    {
        [DataMember(Name = "paging")]
        public FacebookPaging Paging { get; set; }

        public bool ShowPrevious { get; set; }

        public bool ShowNext { get; set; }
    }
}