﻿using System.Collections.Generic;

namespace TotalCoin.Entidades
{
    public class Categoria : EntidadBase
    {
        #region Constructores

        public Categoria()
        {
        }

        public Categoria(int id, string nombre)
        {
            Id = id;
            Name = nombre;
        }

        #endregion Constructores

        public string Name { get; set; }

        public Categoria ParentCategory { get; set; }

        public List<Categoria> Subcategories { get; set; }
    }
}