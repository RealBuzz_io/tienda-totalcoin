﻿using System.Collections.Generic;

namespace TotalCoin.Entidades
{
    public class FormaDePago : EntidadBase
    {
        #region Constructores

        public FormaDePago()
        {
        }

        public FormaDePago(int id, string nombre)
        {
            Name = nombre;
            Id = id;
        }

        #endregion Constructores

        public string Name { get; set; }

        public virtual IList<Producto> Products { get; set; }
    }
}