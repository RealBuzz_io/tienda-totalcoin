﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotalCoin.Entidades
{
    public class Merchant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
