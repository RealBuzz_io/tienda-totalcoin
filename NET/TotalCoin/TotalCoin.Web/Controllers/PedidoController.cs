﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TotalCoin.Entidades.Facebook;
using TotalCoin.ILogica;
using TotalCoin.Logica;
using TotalCoin.Web.Models;

namespace TotalCoin.Web.Controllers
{
    public class PedidoController : Controller
    {
        private IPedidoLogica _pedidoLogica;

        #region Constructores

        public PedidoController(IPedidoLogica pedidoLogica)
        {
            _pedidoLogica = pedidoLogica;
        }

        public PedidoController()
            : this(new PedidoLogica())
        {
        }

        #endregion Constructores

        public ActionResult Confirmar(int id)
        {
            try
            {
                var isOk = _pedidoLogica.Confirmar(id);

                return Json(new { IsOk = isOk }, JsonRequestBehavior.AllowGet);                
            }
            catch (Exception e)
            {
                return Json(new { IsOk = false, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Cancelar(int id)
        {
            try
            {
                var isOk = _pedidoLogica.Cancelar(id);

                return Json(new { IsOk = isOk }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { IsOk = false, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}