﻿using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TotalCoin.ConfigurationManager;
using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.Logica;
using TotalCoin.Web.Models;

namespace TotalCoin.Web.Controllers
{
    public class ProductoController : Controller
    {
        private IProductoLogica _productoLogica;
        private ICategoriaLogica _categoriaLogica;
        private IFormaDePagoLogica _formaDePagoLogica;

        #region Constructores

        public ProductoController(IProductoLogica productoLogica, ICategoriaLogica categoriaLogica, IFormaDePagoLogica formaDePagoLogica)
        {
            _productoLogica = productoLogica;
            _categoriaLogica = categoriaLogica;
            _formaDePagoLogica = formaDePagoLogica;
        }

        public ProductoController()
            : this(new ProductoLogica(), new CategoriaLogica(), new FormaDePagoLogica())
        {
        }

        #endregion Constructores

        #region Actions

        public ActionResult Index(ListarProductosModel model)
        {
            try
            {
                var productos = new List<Producto>();

                if (Session["TiendaId"] != null)
                {
                    int tiendaId = Convert.ToInt32(Session["TiendaId"]);
                    productos = _productoLogica.GetByTienda(tiendaId).ToList();

                    foreach (Producto prod in productos)
                    {
                        prod.Category.Subcategories = _categoriaLogica.GetAllSubcategorias(prod.Category.Id).ToList();
                    }

                    var cantProductos = 5;
                    int.TryParse(CloudConfigurationManager.GetSetting(ConfigurationNames.CantidadDeProductosPorPaginaEnAdmin), out cantProductos);
                    
                    model.Productos = ToModel(_productoLogica.GetPaginado(tiendaId, 1, cantProductos).ToList());
                    model.Categorias = ToModel(_categoriaLogica.GetAllCategorias());
                    model.TiendaId = tiendaId;
                    model.Pagina = 1;
                    model.CantPaginas = _productoLogica.GetCantidadDePaginas(tiendaId, cantProductos);

                    return View(model);
                }
                else
                    return RedirectToAction("Index", "Tienda");
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult GuardarProducto(ProductoModel model)
        {
            try
            {
                if (Session["TiendaId"] != null)
                {
                    int tiendaId = Convert.ToInt32(Session["TiendaId"]);

                    model.TiendaId = tiendaId;
                    var producto = ToEntidad(model);

                    producto = _productoLogica.Guardar(producto);

                    model = new ProductoModel(producto);

                    return Json(new { success = true, idProductoInsertado = model.Id });
                }
                else
                    return RedirectToAction("Index", "Tienda");
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        public ActionResult GetCategoriasYFormasDePago()
        {
            try
            {
                var categorias = ToModel(_categoriaLogica.GetAllCategorias());
                var formasDePago = ToModel(_formaDePagoLogica.GetAll());

                return Json(new { categorias = categorias, formasDePago = formasDePago }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }


        public ActionResult GetCategoriasConProductos()
        {
            try
            {
                var productos = new List<Producto>();
                var categorias = new List<Categoria>();

                if (Session["TiendaIdDesdeFB"] != null)
                {
                    int tiendaId = Convert.ToInt32(Session["TiendaIdDesdeFB"]);
                    productos = _productoLogica.GetByTienda(tiendaId).ToList();

                    foreach (Producto prod in productos)
                    {
                        //Si aun no la agregué, la agrego
                        if (categorias.Count(x => x.Id == prod.Category.Id) == 0)
                        {
                            prod.Category.Subcategories.Clear();
                            prod.Category.Subcategories.Add(prod.Subcategory);
                            categorias.Add(prod.Category);
                        } 
                        else
                        {
                            //No agrego la categoria, pero me fijo si esa subcategoria ya la habia agregado. 
                            int pos = categorias.FindIndex(x => x.Id == prod.Category.Id);
                            if(categorias[pos].Subcategories.Count(x => x.Id == prod.Subcategory.Id) == 0) {
                                //Si no tenia la categoria, la agrego
                                categorias[pos].Subcategories.Add(prod.Subcategory);
                            }
                        }
                    }

                    //Ordeno las subcategorias de cada categoria por orden alfabetico
                    foreach (Categoria categ in categorias)
                    {
                        categ.Subcategories = categ.Subcategories.OrderBy(x => x.Name).ToList();
                    }
                }
                return Json(new { categorias = ToModel(categorias.OrderBy(x => x.Name).ToList()) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        public ActionResult GetProductos(int? pagina, string busqueda)
        {
            try
            {
                var cantProductos = 5;
                int.TryParse(CloudConfigurationManager.GetSetting(ConfigurationNames.CantidadDeProductosPorPaginaEnAdmin), out cantProductos);
                if (Session["TiendaId"] == null)
                    throw new ValidationException("Tienda invalida, por favor refresque la pagina.");

                var tiendaId = Convert.ToInt32(Session["TiendaId"].ToString());

                if (!pagina.HasValue)
                    pagina = 1;

                var productos = ToModel(_productoLogica.GetPaginado(tiendaId, pagina.Value, cantProductos, busqueda, null));
                var cantPaginas = _productoLogica.GetCantidadDePaginas(tiendaId, cantProductos, busqueda, null);

                return Json(new { success = true, productos = productos, paginaActual = pagina.Value, cantidadPaginas = cantPaginas }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EliminarProducto(int id)
        {
            try
            {
                _productoLogica.Eliminar(id);

                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        public ActionResult GuardarImagen()
        {
            bool guardada = true;
            string fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase imagen = Request.Files[fileName];
                    if (imagen != null && imagen.ContentLength > 0)
                        fName = GuardarImagen(imagen);
                }
            }
            catch (Exception)
            {
                guardada = false;
            }

            if (guardada)
                return Json(new { success = true, Imagen = fName });

            return Json(new { success = false, Message = "Error al guardar la imagen" });
        }

        public ActionResult Detalle(int? id, string fanpageId = "0", bool enTienda = false)
        {
            try
            {
                if (id.HasValue)
                {
                    var producto = _productoLogica.GetById(id.Value);
                    ProductoModel productoModel = ToModel(producto);
                    productoModel.AppID = CloudConfigurationManager.GetSetting(ConfigurationNames.FacebookAppId);
                    productoModel.fanpageId = fanpageId;
                    productoModel.EnTienda = enTienda;
                        
                    if (!producto.Deleted)
                    {
                        if (productoModel.Imagenes.Count == 0)
                        {
                            productoModel.Imagenes.Add("");
                        }
                        return View("DetalleProducto", productoModel);
                    }
                    else 
                    {
                        return View("Eliminado", productoModel);
                    }
                }
                else 
                {
                    //Canvase root app
                    return View("../External/AplicacionRoot");
                }
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult Comprar(int productoId, int cantidad)
        {
            try
            {
                var producto = _productoLogica.GetById(productoId);

                var redirectUrl = _productoLogica.GenerarPedido(producto, cantidad);

                return Json(new { success = true, redirectUrl = redirectUrl });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        #endregion Actions

        #region Metodos privados

        private string GuardarImagen(HttpPostedFileBase imagen)
        {
            //var ruta = string.Format("{0}/{1}{2}", ConfigurationManager.AppSettings.Get("RepositorioDeImagenesDeProducto"), Guid.NewGuid(), Path.GetExtension(imagen.FileName));

            //imagen.SaveAs(Server.MapPath(ruta));

            //return string.Concat(ConfigurationManager.AppSettings.Get("EnvironmentUrl"), ruta);

            var ruta = String.Format("{0}{1}", Guid.NewGuid(), Path.GetExtension(imagen.FileName));
            var storageHelper = new StorageHelper();
            imagen.InputStream.Position = 0;
            storageHelper.SaveFile(ruta, imagen.InputStream);
            return storageHelper.GetPath(ruta);       
        }

        #region ToModel

        private IList<ProductoModel> ToModel(IList<Producto> productos)
        {
            return productos.Select(p => new ProductoModel(p)).ToList();
        }

        private ProductoModel ToModel(Producto producto)
        {
            return new ProductoModel(producto);
        }

        private List<CategoriaModel> ToModel(IList<Categoria> categorias)
        {
            return categorias.Select(c => new CategoriaModel(c)).ToList();
        }

        private List<FormaDePagoModel> ToModel(IList<FormaDePago> formaDePago)
        {
            return formaDePago.Select(c => new FormaDePagoModel(c)).ToList();
        }

        #endregion ToModel

        #region ToEntidad

        private Producto ToEntidad(ProductoModel model)
        {
            var producto = new Producto(model.Nombre, model.Descripcion, ToEntidad(model.Categoria), ToEntidad(model.Subcategoria), model.Codigo, model.Precio, model.Stock, ToEntidad(model.Imagenes), ToEntidad(model.FormasDePago), model.TiendaId);

            if (model.Id.HasValue)
                producto.Id = model.Id.Value;

            return producto;
        }

        private IList<FormaDePago> ToEntidad(IList<FormaDePagoModel> list)
        {
            return list != null ? list.Select(fp => new FormaDePago(fp.Id, fp.Nombre)).ToList() : null;
        }

        private IList<Imagen> ToEntidad(IList<string> list)
        {
            var toReturn = new List<Imagen>();

            if(list != null)
                for (var i = 0; i < list.Count; i++)
                {
                    if (!string.IsNullOrEmpty(list[i]))
                        toReturn.Add(new Imagen(list[i], i + 1));
                }

            return toReturn;
        }

        private Categoria ToEntidad(CategoriaModel categoriaModel)
        {
            return categoriaModel != null ? new Categoria(categoriaModel.Id, categoriaModel.Nombre) : null;
        }

        #endregion ToEntidad

        #endregion Metodos privados
    }
}