﻿using Microsoft.Azure;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TotalCoin.ConfigurationManager;
using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.Logica;
using TotalCoin.Web.Models;

namespace TotalCoin.Web.Controllers
{
    public class TiendaController : Controller
    {
        #region Fields

        private ITiendaLogica _tiendaLogica;
        private IFanpageLogica _fanpageLogica;
        private ICategoriaLogica _categoriaLogica;
        private IProductoLogica _productoLogica;

        #endregion Fields

        #region Constructores

        public TiendaController()
            : this(new TiendaLogica(), new FanpageLogica(), new ProductoLogica(), new CategoriaLogica())
        {
        }

        public TiendaController(ITiendaLogica tiendaLogica, IFanpageLogica fanpageLogica, IProductoLogica productoLogica, ICategoriaLogica categoriaLogica)
        {
            _tiendaLogica = tiendaLogica;
            _fanpageLogica = fanpageLogica;
            _productoLogica = productoLogica;
            _categoriaLogica = categoriaLogica;
        }

        #endregion Constructores

        #region Actions

        public ActionResult Index(string appKey, string email)
        {
            ViewBag.Email = email;
            ViewBag.AppKey = appKey;

            //El loader solo devuelve un loading, mientras se pide el AccessToken y obtiene los datos de la tienda
            return View("Loading");
        }

        public ActionResult WarmupEF() { 
            //Debido a tardanzas en EntityFramework al cargar por primera vez el modelo, se realizó este método que se ejecuta mientras está el loading.

            _tiendaLogica.WarmUpEF();
            return Json(new { success = true });
        }

        public ActionResult InitStore(string appKey, string email)
        {
            try
            {
                if (string.IsNullOrEmpty(appKey) && Session["AppKey"] != null)
                    appKey = Session["AppKey"].ToString();

                if (string.IsNullOrEmpty(email) && Session["Email"] != null)
                    email = Session["Email"].ToString();

                var tienda = _tiendaLogica.GetTienda(new Guid(appKey), email);

                Session["Email"] = email;
                Session["AppKey"] = appKey;

                if (tienda == null)
                    return View("Index");
                else
                {
                    Session["TiendaId"] = tienda.Id;
                    return RedirectToAction("Administrar");
                }
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }   
        }

        public ActionResult EditarTienda()
        {
            try
            {
                var model = new CrearTiendaModel();
                int tiendaId;

                if (Session["TiendaId"] != null && int.TryParse(Session["TiendaId"].ToString(), out tiendaId))
                {
                    model = ToModel(_tiendaLogica.GetById(tiendaId));
                    //Verifico si es el mismo email con el que intentó entrar. Por si estaba en una tienda, y después vio otra
                    if (Session["Email"] != null && Session["Email"].ToString() != model.Email) {
                        model = new CrearTiendaModel();
                    }
                }

                //Imagen default
                if(string.IsNullOrEmpty(model.ArchivoImagen))
                    model.ArchivoImagen  = "https://az653149.vo.msecnd.net/static/totalcoinstore/imagenes_tienda/tienda_default.png";
                
                //Get Merchants
                string email = Session["Email"].ToString();
                Guid apiKey = new Guid(Session["AppKey"].ToString());
                var merchants = _tiendaLogica.GetMerchants(apiKey, email);
                model.Merchants = ToModel(merchants).ToList();

                return View(model);
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult Administrar(bool nombreEditado = false)
        {
            int tiendaId;

            if (Session["TiendaId"] != null && int.TryParse(Session["TiendaId"].ToString(), out tiendaId))
                return View(new AdministrarTiendaModel(tiendaId, false, nombreEditado));

            return RedirectToAction("StartStore");
        }

        public ActionResult GuardarTienda(CrearTiendaModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    string email = Session["Email"].ToString();
                    Guid apiKey = new Guid(Session["AppKey"].ToString());
                    var merchants = _tiendaLogica.GetMerchants(apiKey, email);
                    model.Merchants = ToModel(merchants).ToList();
                    return View("EditarTienda", model);
                }

                var rutaImagen = model.Imagen != null ? GuardarImagen(model.Imagen) : model.ArchivoImagen;

                var tienda = ToEntidad(model, rutaImagen);

                tienda = _tiendaLogica.Guardar(tienda);

                Session["TiendaId"] = tienda.Id;

                if (model.NuevaTienda)
                    return RedirectToAction("Index", "Producto", new { NuevaTienda = model.Id == null });
                else
                    return RedirectToAction("Administrar", new { nombreEditado = true });
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult VerTienda()
        {
            try
            {
                ListarProductosModel model = new ListarProductosModel();
                Tienda tienda = new Tienda();

                var lPid = "";
                var appData = "";
                if (Request.Params["signed_request"] != null)
                {
                    string payload = Request.Params["signed_request"].Split('.')[1];
                    var encoding = new UTF8Encoding();
                    var decodedJson = payload.Replace("=", string.Empty).Replace('-', '+').Replace('_', '/');
                    var base64JsonArray = Convert.FromBase64String(decodedJson.PadRight(decodedJson.Length + (4 - decodedJson.Length % 4) % 4, '='));
                    var json = encoding.GetString(base64JsonArray);
                    var o = JObject.Parse(json);
                    lPid = Convert.ToString(o.SelectToken("page.id")).Replace("\"", "");
                    var lLiked = Convert.ToString(o.SelectToken("page.liked")).Replace("\"", "");
                    var lUserId = Convert.ToString(o.SelectToken("user_id")).Replace("\"", "");
                    appData = Convert.ToString(o.SelectToken("app_data")).Replace("\"", "");

                    tienda = _tiendaLogica.GetByFanpageId(lPid);
                    Session["TiendaIdDesdeFB"] = tienda.Id;
                    Session["page_id"] = lPid;
                }

                if (appData != "") {
                    var producto = _productoLogica.GetById(Convert.ToInt32(appData));
                    ProductoModel productoModel = ToModel(producto);
                    return View("../Producto/DetalleProducto", productoModel);
                }

                var productos = new List<Producto>();

                int tiendaId = tienda.Id;
                
                if (tienda.Id == 0)
                {
                    tiendaId = Convert.ToInt32(Session["TiendaIdDesdeFB"]);
                    lPid = Session["page_id"].ToString();
                }

                model.PageID = lPid;
                model.AppID = CloudConfigurationManager.GetSetting(ConfigurationNames.FacebookAppId);
                var cantProductos = 10;
                int.TryParse(CloudConfigurationManager.GetSetting(ConfigurationNames.CantidadDeProductosPorPagina), out cantProductos);
                model.Pagina = 1;
                model.CantPaginas = _productoLogica.GetCantidadDePaginas(tiendaId, cantProductos);
                productos = _productoLogica.GetPaginado(tiendaId, 1, cantProductos).ToList();

                foreach (Producto prod in productos)
                {
                    prod.Category.Subcategories = _categoriaLogica.GetAllSubcategorias(prod.Category.Id).ToList();
                }

                model.Productos = ToModel(productos);

                return View("ListadoProductos", model);
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult GetProductos(int? pagina, string busqueda, int? categoriaId) {
            try
            {
                var cantProductos = 10;
                int.TryParse(CloudConfigurationManager.GetSetting(ConfigurationNames.CantidadDeProductosPorPagina), out cantProductos); 
                if (Session["TiendaIdDesdeFB"] == null)
                    throw new ValidationException("Tienda invalida, por favor refresque la pagina.");

                var tiendaId = Convert.ToInt32(Session["TiendaIdDesdeFB"].ToString());

                if (!pagina.HasValue)
                    pagina = 1;
                
                var productos = ToModel(_productoLogica.GetPaginado(tiendaId, pagina.Value, cantProductos, busqueda, categoriaId));
                var cantPaginas = _productoLogica.GetCantidadDePaginas(tiendaId, cantProductos, busqueda, categoriaId);

                return Json(new { success = true, productos = productos, paginaActual = pagina.Value, cantidadPaginas = cantPaginas }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e) {
                return Json(new { success = false, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        
        }

        #endregion Actions

        #region Metodos privados

        private IList<ProductoModel> ToModel(IList<Producto> productos)
        {
            return productos.Select(p => new ProductoModel(p)).ToList();
        }

        private IList<MerchantModel> ToModel(IList<Merchant> merchant)
        {
            return merchant.Select(p => new MerchantModel(p)).ToList();
        }

        private ProductoModel ToModel(Producto producto)
        {
            return new ProductoModel(producto);
        }

        private string GuardarImagen(HttpPostedFileBase imagen)
        {
            //var ruta = string.Format("{0}/{1}{2}", ConfigurationManager.AppSettings.Get("RepositorioDeImagenesDeTienda"), Guid.NewGuid().ToString(), Path.GetExtension(imagen.FileName));
            //imagen.SaveAs(Server.MapPath(ruta));
            //return string.Concat(ConfigurationManager.AppSettings.Get("EnvironmentUrl"), ruta);
            var ruta = String.Format("{0}{1}", Guid.NewGuid(), Path.GetExtension(imagen.FileName));
            var storageHelper = new StorageHelper();
            imagen.InputStream.Position = 0;

            storageHelper.SaveFile(ruta, imagen.InputStream);
            return storageHelper.GetPath(ruta);           
        }

        private CrearTiendaModel ToModel(Tienda tienda)
        {
            return new CrearTiendaModel(tienda.Id.ToString(), tienda.Name, tienda.Image, tienda.Email, tienda.AppKey, tienda.MerchantId);
        }

        private Tienda ToEntidad(CrearTiendaModel model, string rutaImagen)
        {
            int id;
            bool parsed = int.TryParse(model.Id, out id);

            var email = Session["Email"].ToString();
            var appKey = Session["AppKey"].ToString();

            if (parsed)
                return new Tienda(id, model.Nombre, rutaImagen, email, appKey, model.SelectedMerchantId);

            return new Tienda(model.Nombre, rutaImagen, email, appKey, model.SelectedMerchantId);
        }

        #endregion Metodos privados
    }
}