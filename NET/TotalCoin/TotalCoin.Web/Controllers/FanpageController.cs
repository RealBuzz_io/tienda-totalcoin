﻿using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TotalCoin.ConfigurationManager;
using TotalCoin.Entidades.Facebook;
using TotalCoin.ILogica;
using TotalCoin.Logica;
using TotalCoin.Web.Models;

namespace TotalCoin.Web.Controllers
{
    public class FanpageController : Controller
    {
        private IFanpageLogica _fanpageLogica;

        #region Constructores

        public FanpageController(IFanpageLogica fanpageLogica)
        {
            _fanpageLogica = fanpageLogica;
        }

        public FanpageController()
            : this(new FanpageLogica())
        {
        }

        #endregion Constructores

        #region Actions

        public ActionResult Index(FacebookCredentialsModel fbModel)
        {
            try
            {
                if (fbModel != null && Session["TiendaId"] != null)
                {
                    Session["FBToken"] = fbModel.AccessToken;
                    Session["SignedRequest"] = fbModel.SignedRequest;
                    Session["UserId"] = fbModel.UserId;

                    var fanpages = _fanpageLogica.GetByAccessToken(fbModel.AccessToken, CloudConfigurationManager.GetSetting(ConfigurationNames.FacebookAppId));

                    var model = new AdministrarFanpagesModel();
                    model.Fanpages = ToModel(fanpages);
                    model.NuevaTienda = fbModel.NuevaTienda;

                    return View(model);
                }
                else
                    return RedirectToAction("Index", "Tienda");
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult Actualizar(FacebookCredentialsModel fbModel)
        {
            try
            {
                if (fbModel != null && Session["TiendaId"] != null)
                {
                    Session["FBToken"] = fbModel.AccessToken;
                    Session["SignedRequest"] = fbModel.SignedRequest;
                    Session["UserId"] = fbModel.UserId;

                    _fanpageLogica.ActualizarFanpages(fbModel.AccessToken, CloudConfigurationManager.GetSetting(ConfigurationNames.FacebookAppId), Convert.ToInt32(Session["TiendaId"].ToString()));

                    return RedirectToAction("Administrar", "Tienda");
                }
                else
                    return RedirectToAction("Index", "Tienda");
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult Guardar(IList<FanpageModel> Fanpages)
        {
            try
            {
                if (Fanpages != null && Session["TiendaId"] != null)
                {
                    var fanpages = ToEntity(Fanpages);

                    _fanpageLogica.Guardar(fanpages, CloudConfigurationManager.GetSetting(ConfigurationNames.FacebookAppId), Convert.ToInt32(Session["TiendaId"].ToString()));
                }

                return RedirectToAction("Administrar", "Tienda", new { id = Session["TiendaId"].ToString() });
            }
            catch (Exception e)
            {
                return View("Error", "", e.Message);
            }
        }

        public ActionResult Login(bool nuevaTienda = false) {
            return View(new AdministrarFanpagesModel() { NuevaTienda = nuevaTienda }); 
        }

        #endregion Actions

        #region Metodos privados

        private IList<FanpageModel> ToModel(IList<Fanpage> fanpages)
        {
            return fanpages.Select(fp => new FanpageModel(fp)).ToList();
        }

        private IList<Fanpage> ToEntity(IList<FanpageModel> fanpages)
        {
            return fanpages.Select(f => new Fanpage(f.Id, f.Nombre, f.Integrada, f.PageToken)).ToList();
        }

        #endregion Metodos privados
    }
}