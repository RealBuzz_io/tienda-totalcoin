﻿var module = angular.module('TiendaTC.directivesFB', []);

module.directive('fixMargin', function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element) {
            $(element).load(function () {
                var h = $(this).height();
                var w = $(this).width();
                var pw = $(this).parent().width();
                $(this).css('margin-top', +h / -2 + "px");
                $(this).css('margin-left', +(pw-w) / 2 + "px");
            });
        }
    };
});

module.directive('fbLike', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            'url': '=fbLike',
            'fbId': '=',
            'productId': '=',
            'appId': '='
        },
        link: function (scope, element) {
            element.attr("data-href", scope.url + scope.appId + '/' + scope.productId + '?fanpageId=' + scope.fbId);
            element.attr("data-layout", "button_count");
            element.attr("data-action", "like");
            element.attr("data-show-faces", "true");
            element.attr("data-share", "false");
            element.addClass("fb-like");

            return typeof FB !== "undefined" && FB !== null ? FB.XFBML.parse(element.parent()[0]) : void 0;

            
        }
    };
});