﻿Dropzone.autoDiscover = false;

var module = angular.module('TiendaTC.directives', []);

module.directive("modalShow", function ($parse) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            //Hide or show the modal
            scope.showModal = function (visible, elem, autoHide) {
                if (!elem)
                    elem = element;

                if (visible) {
                    $(elem).modal("show");
                    if(autoHide)
                        setTimeout(function () { $(elem).modal('hide'); }, 1500);
                }
                else
                    $(elem).modal("hide");
            }



            $.fn.insertAt = function (index, $parent) {
                return this.each(function () {
                    if (index === 0) {
                        $parent.prepend(this);
                    } else {
                        $parent.children().eq(index - 1).after(this);
                    }
                });
            }

            ///ENDFIX

            //Watch for changes to the modal-visible attribute
            scope.$watch(attrs.modalShow, function (newValue, oldValue) {
                scope.showModal(newValue, attrs.$$element, attrs.autoHide);
            });

            //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
            $(element).bind("hide.bs.modal", function () {
                $parse(attrs.modalShow).assign(scope, false);
                if (!scope.$$phase && !scope.$root.$$phase)
                    scope.$apply();

                //Show first tab
                if ($('#TabProducto a:first').length)
                    $('#TabProducto a:first').tab('show');
                
                if (attrs.callCloseFunction) {
                    scope.hideAddDialog();
                }


                ///FIX
                var elements = element.find(".img");
                angular.forEach(elements, function (ele) {
                    var originalPos = $(ele).data("pos");
                    $(ele).insertAt(originalPos, $("#ImgsProducto"));
                });
            });
        }
    };
});

module.directive('dropzone', function () {
    return {
        restrict: 'A',
        scope: {
            'uploadUrl': '=',
            'updateModel': '=',
            'successFunction': '&',
            'cleanThumbnails': '=',
            'setImage': '='
        },
        link: function (scope, element) {
            element.dropzone({
                url: scope.uploadUrl,
                acceptedFiles: 'image/*',
                maxFilesize: 3,
                dictFileTooBig: "El archivo debe pesar menos de 2MB",
                dictResponseError: "Error. Intente nuevamente",
                success: function (file, response) {
                    $(file.previewElement).find(".dz-success-mark")[0].style.opacity = "1";
                    scope.updateModel = response.Imagen;
                    scope.$apply();
                },
                complete: function (file, response) {
                    $(file.previewElement).find(".dz-progress")[0].style.opacity = "0";
                    $(file.previewElement).find(".dz-remove").html("Eliminar");
                    scope.$apply();
                    scope.successFunction()();
                },

                dictDefaultMessage: '<span class="glyphicon glyphicon glyphicon-camera" aria-hidden="true"></span>',
                addRemoveLinks: true,
                maxFiles: 1,
                dictRemoveFile: 'Eliminar'
            });

            scope.$watch("setImage", function (newValue, oldValue) {
                if (newValue) {
                    setTimeout(function () {
                        var dzElement = element.get(0).dropzone;
                        var insertFile = { name: newValue, size: 12345 };
                        dzElement.removeAllFiles(true);
                        dzElement.options.addedfile.call(dzElement, insertFile);
                        dzElement.options.thumbnail.call(dzElement, insertFile, newValue);
                        dzElement.files.push(insertFile);
                        dzElement.emit("complete", insertFile);

                    
                        scope.updateModel = newValue;
                        scope.$apply();
                        scope.successFunction()();
                    });
                }
            });

            var dzElement = element.get(0).dropzone;
            dzElement.on("removedfile", function (file) {
                setTimeout(function () {
                    scope.updateModel = '';
                    scope.$apply();
                    scope.successFunction()();
                });
            });

            dzElement.on("thumbnail", function (file) {
                setTimeout(function () {
                    scope.successFunction()();
                });
            });

            scope.$watch("cleanThumbnails", function (newValue, oldValue) {
                if (newValue) {
                    var dzElement = element.get(0).dropzone;
                    dzElement.removeAllFiles();
                    scope.cleanThumbnails = false;
                }
            });
        }
    };
});

module.directive('sortable', function () {
    return {
        restrict: 'A',
        scope: {
            'sortableChildClass': '=',
            'sortFunction': '&'
        },
        link: function (scope, element) {
            var elementId = element.attr("id");
            element.sortable({
                items: scope.sortableChildClass,
                cursor: 'move',
                opacity: 0.5,
                containment: '#'.elementId,
                distance: 20,
                tolerance: 'pointer',
                stop: function (event, ui) {
                    scope.sortFunction()();
                }
            });


            //$(element).insertAfter($("#sortable li:eq(4)"));
            //Go back to original order
        }
    };
});


module.directive('customAutoNumeric', function () {
    return {
        restrict: 'A',
        scope: {
            customAutoNumeric: '=',
            decimales: '=',
        },
        link: function (scope, element, attrs, ngModel) {
            element.autoNumeric('init', {
                aSep: '.',
                aDec: ',',
                vMax: 9999,
                mDec: scope.decimales
            });

            scope.$watch('customAutoNumeric', function (newVal, oldVal) {
                if(newVal != oldVal)
                    element.autoNumeric('set', newVal);
            });
        }
    };
});

module.directive('allowOnlyAlphanumericDash', function () {
    return {
        restrict: 'A',
        scope: {
            textModel: "=allowOnlyAlphanumericDash"
        },
        link: function (scope, element, attrs, ngModel) {
            element.bind('keypress', function (event) {
                var regex = new RegExp("^[- áéíóúüña-zÑÁÉÍÓÚÜA-Z0-9\b]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            element.bind('keypress', function (event) {
                var initVal = $(this).val();
                outputVal = initVal.replace(/[^áéíóúüña-zÑÁÉÍÓÚÜA-Z0-9 -]/g, "");
                if (initVal != outputVal) {
                    $(this).val(outputVal);
                    scope.textModel = outputVal;
                }
            });

            element.bind('blur', function (event) {
                var initVal = $(this).val();
                outputVal = initVal.replace(/[^áéíóúüña-zÑÁÉÍÓÚÜA-Z0-9 -]/g, "");
                if (initVal != outputVal) {
                    $(this).val(outputVal);
                    scope.textModel = outputVal;
                }
            });

        }
    };
});