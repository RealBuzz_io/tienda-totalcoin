﻿var tienda = angular.module('TiendaTC', [
    'TiendaTC.controllers',
    'TiendaTC.directives',
    'ui.utils.masks',
    'TiendaTC.filters'
]);

var tiendaFB = angular.module('TiendaTCFB', [
    'TiendaTC.controllersFB',
    'TiendaTC.directivesFB',
    'TiendaTC.filters'
]);


var filters = angular.module('TiendaTC.filters', []);


filters.filter('formatPrice', ['$filter', '$locale',
    function ($filter, $locale) {
        return function (num) {
            $locale.NUMBER_FORMATS.DECIMAL_SEP = ",";
            $locale.NUMBER_FORMATS.GROUP_SEP = ".";

            return $filter('currency')(num, "");
        };
    }
]);