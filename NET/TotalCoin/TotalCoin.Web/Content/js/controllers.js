﻿var module = angular.module('TiendaTC.controllers', [
    'TiendaTC.services'
]);

module.controller('productoController', function ($scope, productoService, $timeout) {
    $scope.productos = productos || [];
    $scope.productoEdicion = {};
    $scope.loading = false;
    $scope.paginaActual = paginaActual || 1;
    $scope.cantPaginas = cantPaginas || 1;

    productoService.getCategoriasYFormasDePago().then(function (data) {
        if (data) {
            if (data.categorias)
                $scope.categorias = data.categorias;
            else
                $scope.categorias = [];

            if (data.formasDePago)
                $scope.formasDePagoExistentes = data.formasDePago;
            else
                $scope.formasDePagoExistentes = [];

        }
    });

    $scope.getProductos = function (pag, search) {
        productoService.getProductosAdm(pag, search).then(function (data) {
            $scope.productos = data.productos;
            $scope.paginaActual = data.paginaActual;
            $scope.cantPaginas = data.cantidadPaginas;
            $scope.generateCantPagsArray();
        });
    }

    $scope.paginaSiguiente = function () {
        var paginaDeseada = $scope.paginaActual + 1;
        $scope.getProductos(paginaDeseada, $scope.textoBuscar);
    };

    $scope.paginaAnterior = function () {
        var paginaDeseada = $scope.paginaActual - 1;
        $scope.getProductos(paginaDeseada, $scope.textoBuscar);
    };

    $scope.irAPagina = function (numero) {
        $scope.getProductos(numero, $scope.textoBuscar);
    };

    $scope.generateCantPagsArray = function () {
        $scope.cantidadDePaginasArray = new Array($scope.cantPaginas);
    };

    $scope.generateCantPagsArray();

    var timeoutPromise;
    $scope.$watch("searchTextProducts", function (newVal, oldVal) {
        if (newVal != undefined) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
                $scope.getProductos(1, newVal);
            }, 300);   
        }
    });

    $scope.generarFormasDePagoSeleccionadas = function () {
        $scope.productoEdicion.FDPSeleccionadas = {};
        angular.forEach($scope.productoEdicion.FormasDePago, function (formaDePago) {
            $scope.productoEdicion.FDPSeleccionadas[formaDePago.Nombre] = true;
        });
    };

    $scope.getFormaDePagoFromNombre = function (nombre) {
        var obj = _.find($scope.formasDePagoExistentes, function (obj) { return obj.Nombre == nombre });
        return obj ? obj : -1;
    };

    $scope.generarArrayDeFormasDePago = function () {
        $scope.productoEdicion.FormasDePago = [];

        angular.forEach($scope.productoEdicion.FDPSeleccionadas, function (value, key) {
            if ($scope.productoEdicion.FDPSeleccionadas[key])
                $scope.productoEdicion.FormasDePago.push($scope.getFormaDePagoFromNombre(key));
        });

        //delete $scope.productoEdicion.FDPSeleccionadas;
    };

    $scope.generarObjectoParaGuardar = function () {
        $scope.generarArrayDeFormasDePago();
        $scope.productoEdicion.Imagenes = $scope.productoEdicion.imagenesOrdenadas;
        $scope.productoEdicion.Precio = parseFloat($('#PrecioProducto').autoNumeric('get')); //TODO; move this
        $scope.productoEdicion.Stock = parseFloat($('#StockProducto').autoNumeric('get')); //TODO; move this
    };

    $scope.insertarProducto = function () {
        $scope.generarObjectoParaGuardar();
        $scope.loading = true;
        productoService.guardarProducto($scope.productoEdicion).then(function (data) {
            $scope.loading = false;
            if (data && data.success) {
                $scope.productoEdicion.Id = data.idProductoInsertado;
                $scope.getProductos($scope.paginaActual, $scope.textoBuscar);
                //$scope.productos.push($scope.productoEdicion);
                $scope.hideAddDialog();
                $scope.showErrorGuardar = false;
                $scope.showSuccessGuardar = true;
            } else {
                $scope.showErrorGuardar = true;
                $scope.showSuccessGuardar = false;
            }
        },
        function (data) {
            $scope.loading = false;
            $scope.showErrorGuardar = true;
            $scope.showSuccessGuardar = false;
        });
    };

    $scope.actualizarTabla = function () {
        var pos = _.findIndex($scope.productos, function (obj) { return obj.Id == $scope.productoEdicion.Id });
        $scope.productos[pos] = $scope.productoEdicion;
    };

    $scope.editarProducto = function () {
        $scope.generarObjectoParaGuardar();
        $scope.loading = true;
        productoService.guardarProducto($scope.productoEdicion).then(function (data) {
            $scope.loading = false;
            if (data && data.success) {
                //$scope.actualizarTabla();
                $scope.getProductos($scope.paginaActual, $scope.textoBuscar);
                $scope.hideAddDialog();
                $scope.showErrorGuardar = false;
                $scope.showSuccessGuardar = true;
            } else {
                $scope.showErrorGuardar = true;
                $scope.showSuccessGuardar = false;
            }
        },
        function (data) {
            $scope.loading = false;
            $scope.showErrorGuardar = true;
            $scope.showSuccessGuardar = false;
        });
    };

    $scope.eliminarProducto = function () {
        productoService.eliminarProducto($scope.productoAEliminar.Id).then(function (data) {
            if (data && data.success) {
                $scope.getProductos($scope.paginaActual, $scope.textoBuscar);
                $scope.showErrorEliminar = false;
                $scope.showSuccessEliminar = true;
                $scope.hideRemoveDialog();
            } else {
                $scope.showErrorEliminar = true;
                $scope.showSuccessEliminar = false;
            }
        },
        function (data) {
            $scope.showErrorEliminar = true;
            $scope.showSuccessEliminar = false;
        });
    };

    $scope.$watch("productoEdicion.FDPSeleccionadas", function (newVal, oldVal) {

        if (!$scope.productoEdicion)
            return;

        var atLeastOne = false;
        angular.forEach($scope.productoEdicion.FDPSeleccionadas, function (value, key) {
            if ($scope.productoEdicion.FDPSeleccionadas[key])
                atLeastOne = true;
        });

        if (atLeastOne) {
            $scope.formFormasDePago.formaPago.$setValidity("minimoRequerido", true);
        }
        else {
            $scope.formFormasDePago.formaPago.$setValidity("minimoRequerido", false);
            if (newVal) //Not first time
                $scope.formFormasDePago.formaPago.$setDirty();
        }
    }, true);

    $scope.resizeImage = function () {
        if ($(window).width() < 720) {
            $("#ImgsProducto .img").css("height", $("#ImgsProducto .img").width());
            $("#ImgsProducto .img").css("line-height", $("#ImgsProducto .img").width() + "px");
            $(".dz-image").each(function (index) {
                $(this).css("height", $("#ImgsProducto .img").width() - 4);
                $(this).css("width", $("#ImgsProducto .img").width());
            });
            $(".dropzone .dz-preview").css("min-height", "initial");
            $("#ImgsProducto .img img").css("height", $("#ImgsProducto .img").width() - 4);
        } else {
            $(".dz-image").each(function (index) {
                $(this).css("width", $("#ImgsProducto .img").width());
                $("img", this).css("width", $("#ImgsProducto .img").width());
            });
            $(".dropzone .dz-preview").css("min-height", $("#ImgsProducto .img").width());
        }
    };

    $(window).resize(function () {
        $scope.resizeImage();
    });

    $scope.updateOrdenImagenes = function () {
        $scope.productoEdicion.imagenesOrdenadas = angular.element('.imgSrc').map(function () {
            return $(this).prop("value");
        }).toArray();

        $scope.$apply();

        setTimeout(function () {
            $scope.resizeImage();
            $scope.$apply();
        }); 
    };

    $scope.generarImagenesActuales = function () {
        $scope.productoEdicion.ImagenesEditar = [];
        angular.forEach($scope.productoEdicion.Imagenes, function (imagen) {
            $scope.productoEdicion.ImagenesEditar.push(imagen);
        });

        while ($scope.productoEdicion.ImagenesEditar.length < 5) {
            $scope.productoEdicion.ImagenesEditar.push('');
        }
    };

    $scope.seleccionarCategoriaYSubcategoria = function () {
        var posCategoria = _.findIndex($scope.categorias, function (obj) { return obj.Id == $scope.productoEdicion.Categoria.Id });
        $scope.productoEdicion.Categoria = $scope.categorias[posCategoria];

        var posSubcategoria = _.findIndex($scope.productoEdicion.Categoria.Subcategorias, function (obj) { return obj.Id == $scope.productoEdicion.Subcategoria.Id });
        $scope.productoEdicion.Subcategoria = $scope.productoEdicion.Categoria.Subcategorias[posSubcategoria];
    };

    $scope.selectAllFormasDePago = function () {
        $scope.productoEdicion.FDPSeleccionadas = {};
        angular.forEach($scope.formasDePagoExistentes, function (element) {
            $scope.productoEdicion.FDPSeleccionadas[element.Nombre] = true;
        });
    };

    $scope.setEmptyImagesArray = function () {
        $scope.productoEdicion.imagenesOrdenadas = angular.element('.imgSrc').map(function () {
            return $(this).prop("value");
        }).toArray();

        setTimeout(function () {
            $scope.resizeImage();
        }, 1500);
    };

    $scope.editarProductoModal = function (producto) {
        $scope.productoEdicion = angular.copy(producto);
        $scope.seleccionarCategoriaYSubcategoria();
        $scope.generarImagenesActuales();
        $scope.generarFormasDePagoSeleccionadas();
        $scope.showAddDialog = true;
    };

    $scope.agregarProductoModal = function () {
        $scope.productoEdicion = {};
        $scope.selectAllFormasDePago();
        $scope.productoEdicion.imagenes = { imagenUno: '', imagenDos: '', imagenTres: '', imagenCuatro: '', imagenCinco: '' };
        $scope.setEmptyImagesArray();
        $scope.showAddDialog = true;
    };

    $scope.eliminarProductoModal = function (producto) {
        $scope.productoAEliminar = producto;
        $scope.showRemoveDialog = true;
    };

    $scope.hideRemoveDialog = function () {
        $scope.productoAEliminar = null;
        $scope.showRemoveDialog = false;
    };

    $scope.hideAddDialog = function () {
        $scope.cleanThumbnails = true;
        $scope.cleanForms();
        $scope.showAddDialog = false;
        $scope.productoEdicion = null;
        $scope.currentStep = 0;
    };

    $scope.cleanForms = function () {
        $scope.formDescripcion.$setPristine();
        $scope.formCategoria.$setPristine();
        $scope.formImagenes.$setPristine();
        $scope.formFormasDePago.$setPristine();
    };

    $scope.allowSave = function () {
        return $scope.formDescripcion.$valid && $scope.formCategoria.$valid && $scope.formImagenes.$valid && $scope.formFormasDePago.$valid;
    };

    $scope.currentStep = 0;
    $scope.formsAvailable = [
        'formDescripcion',
        'formCategoria',
        'formImagenes',
        'formFormasDePago'
    ];

    $scope.ultimoStep = function () {
        return $scope.currentStep + 1 == $scope.formsAvailable.length;
    };

    $scope.primerStep = function () {
        return $scope.currentStep == 0;
    };

    $scope.allowNext = function () {
        return $scope[$scope.formsAvailable[$scope.currentStep]].$valid;
    };

    $scope.siguienteForm = function () {
        $scope.currentStep += 1;
        var tab = $('#TabProducto a').get($scope.currentStep);
        $(tab).tab('show');
        $scope.resizeImage();
    };

    $scope.anteriorForm = function () {
        $scope.currentStep -= 1;
        var tab = $('#TabProducto a').get($scope.currentStep);
        $(tab).tab('show');
        $scope.resizeImage();
    };
});