﻿var module = angular.module('TiendaTC.controllersFB', [
    'TiendaTC.services'
]);

module.controller('tiendaFacebook', function ($scope, productoService, $timeout) {
    $scope.productos = productos || [];
    $scope.paginaActual = paginaActual || 1;
    $scope.cantPaginas = cantPaginas || 1;
    $scope.pageID = pageID || 1;
    $scope.appID = appID || 1;

    $scope.generateCantPagsArray = function () {
        $scope.cantidadDePaginasArray = new Array($scope.cantPaginas);
    };
    
    $scope.generateCantPagsArray();

    productoService.getCategoriasConProductos().then(function (data) {
        if (data) {
            if (data.categorias)
                $scope.categorias = data.categorias;
        }
    });

    $scope.seleccionarCategoria = function (categoria) {
        $scope.subcategoriaSeleccionada = categoria;
    };

    $scope.paginaSiguiente = function () {
        var paginaDeseada = $scope.paginaActual + 1;
        $scope.getProductos(paginaDeseada, $scope.textoBuscar, $scope.subcategoriaSeleccionada);
    };

    $scope.paginaAnterior = function () {
        var paginaDeseada = $scope.paginaActual - 1;
        $scope.getProductos(paginaDeseada, $scope.textoBuscar, $scope.subcategoriaSeleccionada);
    };

    $scope.irAPagina = function (numero) {
        $scope.getProductos(numero, $scope.textoBuscar, $scope.subcategoriaSeleccionada);
    };

    $scope.getProductos = function (pag, search, categoria) {
        var categoriaId = categoria && categoria.Id || null;
        productoService.getProductos(pag, search, categoriaId).then(function (data) {
            $scope.productos = data.productos;
            $scope.paginaActual = data.paginaActual;
            $scope.cantPaginas = data.cantidadPaginas;
            $scope.generateCantPagsArray();
        });
    }

    var timeoutPromise;
    $scope.$watch("textoBuscar", function (newVal, oldVal) {
        if (newVal != undefined) {
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
                $scope.getProductos(1, $scope.textoBuscar, $scope.subcategoriaSeleccionada);
            }, 300);
        }
    });
    
    $scope.$watch("subcategoriaSeleccionada", function (newVal, oldVal) {
        if (newVal != oldVal) {
            $scope.getProductos(1, $scope.textoBuscar, $scope.subcategoriaSeleccionada);
        }
    });

});

module.controller('detalleProducto', function ($scope, checkoutService) {
    $scope.producto = producto || {};
    $scope.fanpageId = fanpageId || 0;
    $scope.appID = appID || 0;
    $scope.enTienda = enTienda || false;
    $scope.superoImporteMaximo = false;

    $scope.cantidadCombo = [];
    $scope.cantidad = 1;

    for (var i = 1; i <= 10 && i <= $scope.producto.Stock; i++) {
        $scope.cantidadCombo.push(i);
    }

    if ($scope.producto.Stock == 0)
    {
        $scope.cantidadCombo.push("-");
        $scope.cantidad = "-";
    }

    $scope.imagenActual = producto.Imagenes[0];
    

    $scope.producto.FormasDePagoSeleccionadas = {};
    angular.forEach($scope.producto.FormasDePago, function (formaDePago) {
        $scope.producto.FormasDePagoSeleccionadas[formaDePago.Nombre] = true;
    });

    $scope.cambiarImagen = function (imagen) {
        $scope.imagenActual = imagen;
        setTimeout(function () {
            $scope.$apply();
            $('.zoom').trigger('zoom.destroy');
            $('.zoom').zoom();
        });
    };

    $scope.comprar = function ($event) {
        var element = angular.element($event.currentTarget);
        element.addClass("active");
        checkoutService.checkout($scope.producto.Id, $scope.cantidad).then(function (data) {
            if (data && data.success && data.redirectUrl) {
                window.location.href = data.redirectUrl;
            }
        });
    };

    $scope.$watch("cantidad", function (newVal, oldVal) {
        if (newVal) {
            //No permitir comprar si supera mil pesos para la compra. Mover este valor al config
            if (newVal * $scope.producto.Precio > 10000) {
                $scope.superoImporteMaximo = true;
            } else {
                $scope.superoImporteMaximo = false;
            }
        }
    });

    $scope.goToFanpage = function () {
        top.location.href = 'http://www.facebook.com/' + $scope.fanpageId + '?sk=app_' + $scope.appID;
    };

    if ($scope.imagenActual) {
        setTimeout(function () {
            $('.zoom').zoom();
            $('.owl-carousel').owlCarousel({
                margin: 10,
                nav: false,
                dots: true
            });
        });
    }
});