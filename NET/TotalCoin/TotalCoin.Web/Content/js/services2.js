﻿var module = angular.module('TiendaTC.services', []);

module.constant("config", {
    "base_url": "/apps/facebook"
})

module.factory('productoService', function ($http, $q, config) {
    var api = {};

    api.guardarProducto = function (producto) {
        if (!producto.Nombre || !producto.Nombre.length ||
            !producto.Descripcion || !producto.Descripcion.length ||
            !producto.Precio || !producto.Categoria || !producto.Subcategoria
            )
            return $q.reject();

        return $http.post(config.base_url + '/Producto/GuardarProducto', producto).then(function (response) {
            return response.data;
        });
    };

    api.eliminarProducto = function (idProducto) {
        if (!idProducto)
            return $q.reject();

        return $http.post(config.base_url + '/Producto/EliminarProducto/' + idProducto).then(function (response) {
            return response.data;
        });
    };

    api.getCategoriasYFormasDePago = function () {
        return $http.get(config.base_url + '/Producto/GetCategoriasYFormasDePago').then(function (response) {
            return response.data;
        });
    };

    api.getCategoriasConProductos = function () {
        return $http.get(config.base_url + '/Producto/GetCategoriasConProductos').then(function (response) {
            return response.data;
        });
    };
    api.getProductos = function (pagina, texto, categoriaId) {
        return $http.get(config.base_url + '/Tienda/GetProductos/', { params: { pagina: pagina, busqueda: texto, categoriaId: categoriaId } }).then(function (response) {
            return response.data;
        });
    };

    //TODO: Juntar este metodo con el de arriba
    api.getProductosAdm = function (pagina, texto) {
        return $http.get(config.base_url + '/Producto/GetProductos/', { params: { pagina: pagina, busqueda: texto } }).then(function (response) {
            return response.data;
        });
    };

    return api;
});

module.factory('checkoutService', function ($http, $q, config) {
    var api = {};

    api.checkout = function (productoId, cantidad) {
        if (!productoId || !cantidad)
            return $q.reject();

        return $http.post(config.base_url + '/Producto/Comprar', { productoId: productoId, cantidad: cantidad }).then(function (response) {
            return response.data;
        });
    };

    return api;
});