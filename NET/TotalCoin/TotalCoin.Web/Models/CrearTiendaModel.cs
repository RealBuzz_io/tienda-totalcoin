﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using TotalCoin.Web.Shared;

namespace TotalCoin.Web.Models
{
    public class CrearTiendaModel
    {
        #region Constructores

        public CrearTiendaModel()
        {
        }

        public CrearTiendaModel(string id, string nombre, string imagen, string email, string appKey, Guid selectedMerchantId)
        {
            Id = id;
            Nombre = nombre;
            ArchivoImagen = imagen;
            Email = email;
            AppKey = appKey;
            SelectedMerchantId = selectedMerchantId;
        }

        #endregion Constructores

        public string Id { get; set; }

        [Required(ErrorMessage = "El campo es obligatorio")]
        public string Nombre { get; set; }

        [ValidarImagenTienda("ArchivoImagen", ErrorMessage = "Debe seleccionar una imagen JPG, PNG o GIF de 111 x 74 pixeles que no supere 1MB")]
        public HttpPostedFileBase Imagen { get; set; }

        public string Email { get; set; }

        public string AppKey { get; set; }

        public string ArchivoImagen { get; set; }

        public bool NuevaTienda { get { return String.IsNullOrEmpty(Id); } }

        [Required(ErrorMessage = "El campo es obligatorio")]
        public Guid SelectedMerchantId { get; set; }

        public List<MerchantModel> Merchants { get; set; }
    }
}