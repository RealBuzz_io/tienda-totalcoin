﻿using System.Collections.Generic;

namespace TotalCoin.Web.Models
{
    public class AdministrarFanpagesModel
    {
        public IList<FanpageModel> Fanpages { get; set; }

        public bool NuevaTienda { get; set; }

        //public bool Logged { get; set; }
    }
}