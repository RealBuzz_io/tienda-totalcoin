﻿using System.Collections.Generic;

namespace TotalCoin.Web.Models
{
    public class ListarProductosModel
    {
        public int TiendaId { get; set; }

        public IList<ProductoModel> Productos { get; set; }

        public bool NuevaTienda { get; set; }

        public List<CategoriaModel> Categorias { get; set; }

        public List<CategoriaModel> Subcategorias { get; set; }

        public int Pagina{ get; set; }

        public int CantPaginas { get; set; }

        public string PageID { get; set; }

        public string AppID { get; set; }
    }
}