﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.Web.Models
{
    [JsonObject]
    public class MerchantModel
    {
        public MerchantModel()
        {
        }

        public MerchantModel(Merchant merchant)
        {
            Name = merchant.Name;
            Id = merchant.Id;
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }
    }
}