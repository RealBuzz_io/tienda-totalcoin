﻿using TotalCoin.Entidades.Facebook;

namespace TotalCoin.Web.Models
{
    public class FanpageModel
    {
        #region Constructores

        public FanpageModel()
        {
        }

        public FanpageModel(Fanpage fanpage)
        {
            Id = fanpage.Id;
            Nombre = fanpage.Nombre;
            Integrada = fanpage.Integrada;
            PageToken = fanpage.PageToken;
        }

        #endregion Constructores

        public string Id { get; set; }

        public string Nombre { get; set; }

        public string PageToken { get; set; }

        public bool Integrada { get; set; }
    }
}