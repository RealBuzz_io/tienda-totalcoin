﻿namespace TotalCoin.Web.Models
{
    public class FacebookCredentialsModel
    {
        public string AccessToken { get; set; }

        public string SignedRequest { get; set; }

        public string UserId { get; set; }

        public bool NuevaTienda { get; set; }
    }
}