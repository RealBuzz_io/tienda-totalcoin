﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.Web.Models
{
    [JsonObject]
    public class CategoriaModel
    {
        public CategoriaModel()
        {
        }

        public CategoriaModel(Categoria categoria)
        {
            Nombre = categoria.Name;
            Id = categoria.Id;
            Subcategorias = new List<CategoriaModel>();

            if (categoria.Subcategories != null)
            {
                foreach (Categoria subcategoria in categoria.Subcategories)
                {
                    Subcategorias.Add(new CategoriaModel(subcategoria));
                }
            }
        }

        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("subcategorias")]
        public List<CategoriaModel> Subcategorias { get; set; }
    }
}