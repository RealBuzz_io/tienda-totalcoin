﻿namespace TotalCoin.Web.Models
{
    public class AdministrarTiendaModel
    {
        public AdministrarTiendaModel(int tiendaId, bool nuevaTienda, bool nombreEditado)
        {
            TiendaId = tiendaId;
            NuevaTienda = nuevaTienda;
            NombreEditado = nombreEditado;
        }

        public int TiendaId { get; set; }

        public bool NuevaTienda { get; set; }
        
        public bool NombreEditado { get; set; }
    }
}