﻿using Newtonsoft.Json;
using TotalCoin.Entidades;

namespace TotalCoin.Web.Models
{
    [JsonObject]
    public class FormaDePagoModel
    {
        public FormaDePagoModel()
        {
        }

        public FormaDePagoModel(FormaDePago formaDePago)
        {
            Id = formaDePago.Id;
            Nombre = formaDePago.Name;
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("nombre")]
        public string Nombre { get; set; }
    }
}