﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using TotalCoin.Entidades;

namespace TotalCoin.Web.Models
{
    [JsonObject]
    public class ProductoModel
    {
        #region Constructores

        public ProductoModel()
        {
        }

        public ProductoModel(Producto producto)
        {
            Nombre = producto.Name;
            Descripcion = producto.Description;
            Categoria = new CategoriaModel(producto.Category);
            Subcategoria = new CategoriaModel(producto.Subcategory);
            FormasDePago = producto.PaymentMethods.Select(x => new FormaDePagoModel(x)).ToList();
            Precio = producto.Price;
            Imagenes = new List<string>();
            Stock = producto.Stock;
            Codigo = producto.Code;
            foreach (Imagen imagen in producto.Images)
            {
                Imagenes.Add(imagen.Path);
            }
            Id = producto.Id;
            TiendaId = producto.StoreId.Value;
        }

        #endregion Constructores

        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("descripcion")]
        public string Descripcion { get; set; }

        [JsonProperty("stock")]
        public int Stock { get; set; }

        [JsonProperty("codigo")]
        public string Codigo { get; set; }

        [JsonProperty("categoria")]
        public CategoriaModel Categoria { get; set; }

        [JsonProperty("subcategoria")]
        public CategoriaModel Subcategoria { get; set; }

        [JsonProperty("precio")]
        public decimal Precio { get; set; }

        [JsonProperty("imagenes")]
        public IList<string> Imagenes { get; set; }

        [JsonProperty("formasDePago")]
        public IList<FormaDePagoModel> FormasDePago { get; set; }

        [JsonProperty("tienda_id")]
        public int TiendaId { get; set; }

        [JsonProperty("appID")]
        public string AppID { get; set; }

        [JsonProperty("appID")]
        public string fanpageId { get; set; }

        [JsonProperty("enTienda")]
        public bool EnTienda { get; set; }
    }
}