﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace TotalCoin.Web.Shared
{
    public class ValidarImagenTienda : ValidationAttribute
    {
        private string _archivoImagen;

        public ValidarImagenTienda(string archivoImagen)
        {
            _archivoImagen = archivoImagen;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
            {
                var archivoImagen = context.ObjectType.GetProperty(_archivoImagen).GetValue(context.ObjectInstance, null);
                return archivoImagen == null ? new ValidationResult(this.FormatErrorMessage(context.DisplayName)) : null;
            }

            if (file.ContentLength > 1 * 1024 * 1024) //1 MB
                return new ValidationResult(this.FormatErrorMessage(context.DisplayName));

            try
            {
                using (var img = Image.FromStream(file.InputStream))
                {
                    if (!(img.RawFormat.Equals(ImageFormat.Png) || img.RawFormat.Equals(ImageFormat.Gif) || img.RawFormat.Equals(ImageFormat.Jpeg)))
                        return new ValidationResult(this.FormatErrorMessage(context.DisplayName));

                    if (img.Width != 111 || img.Height != 74)
                        return new ValidationResult(this.FormatErrorMessage(context.DisplayName));

                    return null;
                }
            }
            catch { }
            return null;
        }
    }
}