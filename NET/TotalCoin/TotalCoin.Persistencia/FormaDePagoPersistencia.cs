﻿using System.Collections.Generic;
using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.IPersistencia;

namespace TotalCoin.Persistencia
{
    public class FormaDePagoPersistencia : IFormaDePagoPersistencia
    {
        public FormaDePago GetById(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.FormasDePago.FirstOrDefault(f => f.Id == id);
            }
        }

        public IList<FormaDePago> GetAll()
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.FormasDePago.ToList();
            }
        }
    }
}