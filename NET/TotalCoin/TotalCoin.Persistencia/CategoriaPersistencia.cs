﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.IPersistencia;

namespace TotalCoin.Persistencia
{
    public class CategoriaPersistencia : ICategoriaPersistencia
    {
        public IList<Categoria> GetAllCategoria()
        {
            using (EFDbContext db = new EFDbContext())
            {
                IList<Categoria> categorias = db.Categorias.Where(c => c.ParentCategory == null).Include(c => c.Subcategories).ToList();

                return categorias;
            }
        }

        public IList<Categoria> GetAllSubcategoria(int idPadre)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Categorias.Where(c => c.ParentCategory.Id == idPadre).ToList();
            }
        }

        public Categoria GetById(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Categorias.FirstOrDefault(c => c.Id == id);
            }
        }
    }
}