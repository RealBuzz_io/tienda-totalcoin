﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using TotalCoin.Entidades;

namespace TotalCoin.Persistencia
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
            : base("name=DbConnectionString")
        { }

        public DbSet<Tienda> Tiendas { get; set; }

        public DbSet<Producto> Productos { get; set; }

        public DbSet<Categoria> Categorias { get; set; }

        public DbSet<FormaDePago> FormasDePago { get; set; }

        public DbSet<Imagen> Imagenes { get; set; }

        public DbSet<FanpageTienda> FanpagesPorTienda { get; set; }

        public DbSet<Pedido> Pedidos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Tienda
            modelBuilder.Entity<Tienda>().ToTable("Store");
            modelBuilder.Entity<Tienda>().Property(p => p.Id).HasColumnName("StoreId");

            //Categoria
            modelBuilder.Entity<Categoria>().ToTable("Category");
            modelBuilder.Entity<Categoria>().Property(p => p.Id).HasColumnName("CategoryId");

            modelBuilder.Entity<Categoria>()
                .HasOptional(c => c.ParentCategory)
                .WithMany(c => c.Subcategories)
                .Map(cs => cs.MapKey("ParentCategoryId"))
                .WillCascadeOnDelete(false);

            //Imagen
            modelBuilder.Entity<Imagen>().ToTable("ProductImage");
            modelBuilder.Entity<Imagen>().Property(p => p.Id).HasColumnName("ImageId");

            //Producto
            modelBuilder.Entity<Producto>().ToTable("Product");
            modelBuilder.Entity<Producto>().Property(p => p.Id).HasColumnName("ProductId");

            modelBuilder.Entity<Producto>()
                .HasRequired(p => p.Category)
                .WithMany()
                .Map(cs => cs.MapKey("CategoryId"))
                .WillCascadeOnDelete(false); ;

            modelBuilder.Entity<Producto>()
                .HasRequired(p => p.Subcategory)
                .WithMany()
                .Map(cs => cs.MapKey("SubcategoryId"))
                .WillCascadeOnDelete(false); ;

            modelBuilder.Entity<Producto>()
                .HasMany<FormaDePago>(p => p.PaymentMethods)
                .WithMany(c => c.Products)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProductId");
                    cs.MapRightKey("PaymentMethodId");
                    cs.ToTable("ProductPaymentMethod");
                });

            modelBuilder.Entity<Producto>()
                .HasMany<Imagen>(p => p.Images)
                .WithOptional()
                .Map(cs => cs.MapKey("ProductId"))
                .WillCascadeOnDelete(true);

            //FormaDePago
            modelBuilder.Entity<FormaDePago>().ToTable("PaymentMethod");
            modelBuilder.Entity<FormaDePago>().Property(f => f.Id).HasColumnName("PaymentMethodId");

            //FanpageTienda
            modelBuilder.Entity<FanpageTienda>().ToTable("StoreFanpage");
            modelBuilder.Entity<FanpageTienda>().HasKey(f => f.Id);

            modelBuilder.Entity<FanpageTienda>().Property(f => f.Id)
                .HasColumnName("FanpageId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<FanpageTienda>()
                .HasRequired(f => f.Store)
                .WithMany()
                .Map(cs => cs.MapKey("StoreId"))
                .WillCascadeOnDelete(false);

            //Pedido
            modelBuilder.Entity<Pedido>().ToTable("Order");
            modelBuilder.Entity<Pedido>().Property(f => f.Id).HasColumnName("OrderId");
        }
    }
}