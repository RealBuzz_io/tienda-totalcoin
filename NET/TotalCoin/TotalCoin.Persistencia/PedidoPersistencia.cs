﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.IPersistencia;
using TotalCoin.Entidades.Enums;

namespace TotalCoin.Persistencia
{
    public class PedidoPersistencia : IPedidoPersistencia
    {
        public int Guardar(Pedido pedido)
        {
            using (EFDbContext db = new EFDbContext())
            {
                db.Pedidos.Add(pedido);

                db.SaveChanges();

                db.Entry(pedido).GetDatabaseValues();

                return pedido.Id;

            }
        }

        public Pedido GetById(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Pedidos.Where(p => p.Id == id)
                    .Include(p => p.Product).FirstOrDefault();
            }
        }

        public bool Confirmar(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                var pedidoExistente = db.Pedidos.Where(p => p.Id == id).FirstOrDefault();

                pedidoExistente.Status = (int)Estado.Confirmado;

                db.Entry(pedidoExistente).State = EntityState.Modified;

                db.SaveChanges();
            }

            return true;
        }

        public bool Cancelar(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                var pedidoExistente = db.Pedidos.Where(p => p.Id == id).FirstOrDefault();

                pedidoExistente.Status = (int)Estado.Cancelado;

                db.Entry(pedidoExistente).State = EntityState.Modified;

                db.SaveChanges();
            }

            return true;
        }
    }
}