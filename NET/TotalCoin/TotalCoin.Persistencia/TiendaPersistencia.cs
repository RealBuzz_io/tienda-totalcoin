﻿using System;
using System.Data.Entity;
using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.IPersistencia;

namespace TotalCoin.Persistencia
{
    public class TiendaPersistencia : ITiendaPersistencia
    {
        public Tienda Guardar(Tienda tienda)
        {
            using (EFDbContext db = new EFDbContext())
            {
                if (tienda.Id > 0)
                {
                    db.Tiendas.Attach(tienda);
                    db.Entry(tienda).State = EntityState.Modified;
                }
                else
                {
                    db.Tiendas.Add(tienda);
                    db.Entry(tienda).State = EntityState.Added;
                }
                db.SaveChanges();
            }

            return tienda;
        }

        public Tienda Get(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Tiendas.First(t => t.Id == id);
            }
        }

        public Tienda GetByFanpageId(string fanpageId)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.FanpagesPorTienda.Include(x => x.Store).FirstOrDefault(t => t.Id == fanpageId).Store;
            }
        }

        public Tienda GetByEmailAndApiKey(string email, Guid apiKey)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Tiendas.FirstOrDefault(t => t.Email == email && t.AppKey == apiKey.ToString());
            }
        }

        public void WarmUpEF()
        {
            using (EFDbContext db = new EFDbContext())
            {
                Tienda tienda = db.Tiendas.FirstOrDefault();
            }
        }
    }
}