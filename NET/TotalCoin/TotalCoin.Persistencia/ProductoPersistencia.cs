﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.IPersistencia;

namespace TotalCoin.Persistencia
{
    public class ProductoPersistencia : IProductoPersistencia
    {
        public IList<Producto> GetByTiendaId(int tiendaId)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Productos.Where(p => p.StoreId == tiendaId && !p.Deleted)
                    .Include(p => p.Category)
                    .Include(p => p.PaymentMethods)
                    .Include(p => p.Subcategory)
                    .Include(p => p.Images).ToList();
            }
        }

        public IList<Producto> GetByTiendaId(int tiendaId, int start, int limit, string busqueda, int? categoriaId)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Productos
                    .Include(p => p.Category)
                    .Include(p => p.PaymentMethods)
                    .Include(p => p.Subcategory)
                    .Include(p => p.Images)
                    .Where(p => 
                        p.StoreId == tiendaId 
                        && !p.Deleted
                        && (string.IsNullOrEmpty(busqueda) ? true : p.Name.Contains(busqueda)) 
                        && (categoriaId.HasValue ? p.Category.Id.Equals(categoriaId.Value) || p.Subcategory.Id.Equals(categoriaId.Value) : true))
                    .OrderBy(p => p.Id)
                    .Skip(start)
                    .Take(limit).ToList();
            }
        }

        public Producto GetById(int id)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Productos.Where(p => p.Id == id)
                    .Include(p => p.Category)
                    .Include(p => p.PaymentMethods)
                    .Include(p => p.Subcategory)
                    .Include(p => p.Store)
                    .Include(p => p.Images).FirstOrDefault();
            }
        }

        public void Eliminar(Producto producto)
        {
            using (EFDbContext db = new EFDbContext())
            {
                db.Productos.Attach(producto);
                producto.Deleted = true;
                db.SaveChanges();
            }
        }

        public Producto Insert(Producto producto)
        {
            using (EFDbContext db = new EFDbContext())
            {
                db.Productos.Add(producto);
                db.Entry(producto).State = EntityState.Added;

                db.Entry(producto.Subcategory).State = EntityState.Unchanged;
                db.Entry(producto.Category).State = EntityState.Unchanged;

                foreach (var formaDePago in producto.PaymentMethods)
                    db.Entry(formaDePago).State = EntityState.Unchanged;

                db.SaveChanges();
            }

            return producto;
        }

        public Producto Update(Producto producto)
        {
            using (EFDbContext db = new EFDbContext())
            {
                var productoExistente = db.Productos.Where(p => p.Id == producto.Id && !p.Deleted)
                    .Include(p => p.PaymentMethods)
                    .Include(p => p.Images).FirstOrDefault();

                var formasDePagoEliminadas = productoExistente
                    .PaymentMethods
                    .Except(producto.PaymentMethods, new FormaDePagoComparer())
                    .ToList<FormaDePago>();

                var formasDePagoAgregadas = producto
                    .PaymentMethods
                    .Except(productoExistente.PaymentMethods, new FormaDePagoComparer())
                    .ToList<FormaDePago>();

                formasDePagoEliminadas.ForEach(fp => productoExistente.PaymentMethods.Remove(fp));
                foreach (var fp in formasDePagoAgregadas)
                {
                    productoExistente.PaymentMethods.Add(fp);
                    db.Entry(fp).State = EntityState.Unchanged;
                }

                var imagenesEliminadas = productoExistente
                    .Images
                    .Except(producto.Images, new ImagenComparer())
                    .ToList();
                imagenesEliminadas.ForEach(i => productoExistente.Images.Remove(i));

                var imagenesAgregadas = producto
                    .Images
                    .Except(productoExistente.Images, new ImagenComparer())
                    .ToList();

                foreach (var imagen in imagenesAgregadas)
                {
                    productoExistente.Images.Add(imagen);
                    db.Entry(imagen).State = EntityState.Added;
                }

                productoExistente.Name = producto.Name;
                productoExistente.Description = producto.Description;
                productoExistente.Price = producto.Price;
                productoExistente.Stock = producto.Stock;
                productoExistente.Code = producto.Code;
                productoExistente.Category = db.Categorias.FirstOrDefault(c => c.Id == producto.Category.Id);
                productoExistente.Subcategory = db.Categorias.FirstOrDefault(c => c.Id == producto.Subcategory.Id);

                db.Entry(productoExistente).State = EntityState.Modified;

                db.SaveChanges();
            }

            return producto;
        }


        public int GetCantidadByTienda(int tiendaId, string busqueda, int? categoriaId)
        {
            using (EFDbContext db = new EFDbContext())
            {
                return db.Productos
                    .Include(p => p.Category)
                    .Include(p => p.Subcategory)
                    .Where(p =>
                        p.StoreId == tiendaId
                        && !p.Deleted
                        && (string.IsNullOrEmpty(busqueda) ? true : p.Name.Contains(busqueda))
                        && (categoriaId.HasValue ? p.Category.Id.Equals(categoriaId.Value) || p.Subcategory.Id.Equals(categoriaId.Value) : true))
                    .Count();
            }
        }
    }

    internal class FormaDePagoComparer : EqualityComparer<FormaDePago>
    {
        public override bool Equals(FormaDePago e1, FormaDePago e2)
        {
            return e1.Id.Equals(e2.Id);
        }

        public override int GetHashCode(FormaDePago obj)
        {
            return 0;
        }
    }

    internal class ImagenComparer : EqualityComparer<Imagen>
    {
        public override bool Equals(Imagen e1, Imagen e2)
        {
            return e1.Path.Equals(e2.Path) && e1.Order.Equals(e2.Order);
        }

        public override int GetHashCode(Imagen obj)
        {
            return 0;
        }
    }
}