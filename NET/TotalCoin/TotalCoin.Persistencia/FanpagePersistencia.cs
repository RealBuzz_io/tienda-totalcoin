﻿using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.Entidades.Facebook;
using TotalCoin.IPersistencia;

namespace TotalCoin.Persistencia
{
    public class FanpagePersistencia : IFanpagePersistencia
    {
        public void EliminarFanpage(Fanpage fanpage)
        {
            using (EFDbContext db = new EFDbContext())
            {
                var fanpageTienda = db.FanpagesPorTienda.FirstOrDefault(f => f.Id == fanpage.Id);
                db.Entry(fanpageTienda).State = System.Data.Entity.EntityState.Deleted;

                db.SaveChanges();
            }
        }

        public void AgregarFanpage(Fanpage fanpage, Tienda tienda)
        {
            using (EFDbContext db = new EFDbContext())
            {
                if (!db.FanpagesPorTienda.Any(f => f.Id.Equals(fanpage.Id)))
                {
                    db.FanpagesPorTienda.Add(new FanpageTienda(fanpage, tienda));
                    db.Entry(tienda).State = System.Data.Entity.EntityState.Unchanged;

                    db.SaveChanges();
                }
            }
        }
    }
}