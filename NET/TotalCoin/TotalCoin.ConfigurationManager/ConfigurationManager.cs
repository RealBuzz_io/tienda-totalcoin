﻿using System;

namespace TotalCoin.ConfigurationManager
{
    public class AppSettings
    {
        public static T Get<T>(string key, T defaultValue)
        {
            string value = System.Web.Configuration.WebConfigurationManager.AppSettings[key];

            return string.IsNullOrEmpty(value) ? defaultValue : (T)Convert.ChangeType(System.Web.Configuration.WebConfigurationManager.AppSettings[key], typeof(T));
        }

        public static string Get(string key)
        {
            return Get<string>(key, string.Empty);
        }
    }
}