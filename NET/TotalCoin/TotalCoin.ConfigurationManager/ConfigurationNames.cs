﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotalCoin.ConfigurationManager
{
    public class ConfigurationNames
    {
        public const string FacebookAppId = "FacebookAppId";
        public const string CantidadDeProductosPorPagina = "CantidadDeProductosPorPagina";
        public const string CantidadDeProductosPorPaginaEnAdmin = "CantidadDeProductosPorPaginaEnAdmin";
        public const string TotalCoinHome = "TotalCoinHome";
    }
}
