﻿using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TotalCoin.ConfigurationManager
{
    public class StorageHelper
    {
        public StorageHelper()
        {
            var storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString"));
            blobClient = storageAccount.CreateCloudBlobClient();
            // Get and create the container
            blobContainer = blobClient.GetContainerReference("totalcoinstore");
        }

        public StorageHelper(string container)
        {
            var storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString"));
            blobClient = storageAccount.CreateCloudBlobClient();
            // Get and create the container
            blobContainer = blobClient.GetContainerReference(container);
        }

        CloudBlobClient blobClient = null;
        CloudBlobContainer blobContainer = null;

        public void SaveFile(string blobName, Stream content)
        {
            // Retrieve reference to a blob 
            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(blobName);
            blockBlob.UploadFromStream(content);
        }

        public string Download(string blobName)
        {
            // Retrieve reference to a blob 
            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(blobName);

            string text;
            using (var memoryStream = new MemoryStream())
            {
                blockBlob.DownloadToStream(memoryStream);
                text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
            }

            return text;
        }

        public MemoryStream GetStream(string blobName)
        {
            // Retrieve reference to a blob 
            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(blobName);
            MemoryStream memoryStream = new MemoryStream();
            blockBlob.DownloadToStream(memoryStream);
            return memoryStream;
        }

        public void Delete(string blobName)
        {
            // Retrieve reference to a blob 
            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(blobName);

            // Delete the blob.
            blockBlob.Delete();
        }

        public string GetPath(string blobName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(blobClient.BaseUri);
            sb.Append("totalcoinstore");
            sb.Append("/");
            sb.Append(blobName);

            return sb.ToString();
        }

        public string GetPath(string container, string blobName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(blobClient.BaseUri);
            sb.Append(container);
            sb.Append("/");
            sb.Append(blobName);

            return sb.ToString();
        }
    }
}
