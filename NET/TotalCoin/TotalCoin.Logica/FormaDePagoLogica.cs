﻿using System.Collections.Generic;
using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.IPersistencia;
using TotalCoin.Persistencia;

namespace TotalCoin.Logica
{
    public class FormaDePagoLogica : IFormaDePagoLogica
    {
        private IFormaDePagoPersistencia _persistencia;

        #region Constructores

        public FormaDePagoLogica(IFormaDePagoPersistencia persistencia)
        {
            _persistencia = persistencia;
        }

        public FormaDePagoLogica()
            : this(new FormaDePagoPersistencia())
        {
        }

        #endregion Constructores

        public FormaDePago GetById(int id)
        {
            return _persistencia.GetById(id);
        }

        public IList<FormaDePago> GetAll()
        {
            return _persistencia.GetAll();
        }
    }
}