﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.IPersistencia;
using TotalCoin.Persistencia;

namespace TotalCoin.Logica
{
    public class ProductoLogica : IProductoLogica
    {
        private IProductoPersistencia _productoPersistencia;
        private IFormaDePagoLogica _formaDePagoLogica;
        private ITiendaLogica _tiendaLogica;
        private IPedidoLogica _pedidoLogica;

        #region Contructores

        public ProductoLogica(IProductoPersistencia productoPersistencia, IFormaDePagoLogica formaDePagoLogica, ITiendaLogica tiendaLogica, IPedidoLogica pedidoLogica)
        {
            _productoPersistencia = productoPersistencia;
            _formaDePagoLogica = formaDePagoLogica;
            _tiendaLogica = tiendaLogica;
            _pedidoLogica = pedidoLogica;
        }

        public ProductoLogica()
            : this(new ProductoPersistencia(), new FormaDePagoLogica(), new TiendaLogica(), new PedidoLogica())
        {
        }

        #endregion Contructores

        public IList<Producto> GetByTienda(int tiendaId)
        {
            return _productoPersistencia.GetByTiendaId(tiendaId);
        }

        public Producto GetById(int id)
        {
            return _productoPersistencia.GetById(id);
        }

        public Producto Guardar(Producto producto)
        {
            Validar(producto);

            if (producto.Id > 0)
                return _productoPersistencia.Update(producto);

            return _productoPersistencia.Insert(producto);
        }

        public void Eliminar(int id)
        {
            var producto = _productoPersistencia.GetById(id);

            if (producto == null)
                throw new ValidationException("Producto inexistente.");

            _productoPersistencia.Eliminar(producto);
        }

        public string GenerarPedido(Producto producto, int cantidad)
        {
            //Get merchant de la tienda
            var merchantId = _tiendaLogica.GetById(producto.StoreId.Value).MerchantId;
            var pedidoId = _pedidoLogica.Nuevo(producto, cantidad);
            var purchaseInformation = GenerarInformacionDeCompra(producto, cantidad, merchantId, pedidoId);

            string url = _tiendaLogica.PerformCheckout(purchaseInformation);

            return url;
        }

        public IList<Producto> GetPaginado(int tiendaId, int pagina, int cantProductos, string busqueda, int? categoriaId)
        {
            var start = (pagina - 1) * cantProductos;
            return _productoPersistencia.GetByTiendaId(tiendaId, start, cantProductos, busqueda, categoriaId);
        }

        public IList<Producto> GetPaginado(int tiendaId, int pagina, int cantProductos)
        {
            return GetPaginado(tiendaId, pagina, cantProductos, null, null);
        }

        public int GetCantidadDePaginas(int tiendaId, int cantProductos)
        {
            var cantTotal = _productoPersistencia.GetByTiendaId(tiendaId).Count;

            return (cantTotal % cantProductos > 0) ? (cantTotal/cantProductos + 1) : cantTotal/cantProductos;
        }

        public int GetCantidadDePaginas(int tiendaId, int cantProductos, string busqueda, int? categoriaId)
        {
            var cantTotal = _productoPersistencia.GetCantidadByTienda(tiendaId, busqueda, categoriaId);

            return (cantTotal % cantProductos > 0) ? (cantTotal / cantProductos + 1) : cantTotal / cantProductos;
        }

        #region Metodos privados

        private void Validar(Producto producto)
        {
            if (string.IsNullOrEmpty(producto.Name))
                throw new ValidationException("El nombre del producto es requerido");

            if (string.IsNullOrEmpty(producto.Description))
                throw new ValidationException("La descripcion del producto es requerida");

            if (!producto.StoreId.HasValue)
                throw new ValidationException("La tienda del producto es requerida");

            if (producto.Category == null)
                throw new ValidationException("La categoria del producto es requerida");

            if (producto.PaymentMethods.Count == 0)
                throw new ValidationException("Debe seleccionar al menos un medio de pago");

            if (producto.Price < 0)
                throw new ValidationException("El precio debe ser mayor o igual a cero");

            if (producto.Stock < 0)
                throw new ValidationException("El stock debe ser mayor o igual a cero");

            if (producto.Subcategory == null)
                throw new ValidationException("La subcategoria del producto es requerida");
        }

        private PurchaseInformation GenerarInformacionDeCompra(Producto producto, int cantidad, Guid merchantId, int pedidoId)
        {
            Guid apiKey = new Guid(producto.Store.AppKey);
            var token = _tiendaLogica.GetAccessToken(apiKey, producto.Store.Email);

            if (!token.IsOk)
                throw new ValidationException("Datos de tienda invalidos");

            var info = new PurchaseInformation();

            info.Amount = producto.Price;
            info.Quantity = cantidad;
            info.Country = "Argentina";
            info.Currency = "ARS";
            info.Description = producto.Name;
            info.FailureURL = "https://facebook.com";
            info.PaymentMethods = GetArrayFormasDePago(producto.PaymentMethods);
            info.PendingURL = "https://facebook.com";
            info.Reference = pedidoId.ToString();
            info.Site = ConfigurationManager.AppSettings.Get("EnvironmentURL");
            info.SuccessURL = "https://facebook.com";
            info.MerchantId = merchantId;
            
            return info;
        }

        private string GetArrayFormasDePago(IList<FormaDePago> list)
        {
            StringBuilder toReturn = new StringBuilder();

            foreach (var fp in list)
            {
                if (toReturn.ToString() == string.Empty)
                    toReturn.Append(fp.Name);
                else
                    toReturn.AppendFormat("|{0}", fp.Name);
            }

            return toReturn.ToString();
        }

        #endregion Metodos privados
    }
}