﻿using System;
using System.Collections.Generic;
using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.IPersistencia;
using TotalCoin.Persistencia;

namespace TotalCoin.Logica
{
    public class TiendaLogica : ITiendaLogica
    {
        private ITiendaPersistencia _tiendaPersistencia;
        private TotalCoin sdk;

        #region Constructores

        public TiendaLogica()
            : this(new TiendaPersistencia())
        {
        }

        public TiendaLogica(ITiendaPersistencia tiendaPersistencia)
        {
            _tiendaPersistencia = tiendaPersistencia;
        }

        #endregion Constructores

        public Tienda GetById(int id)
        {
            return _tiendaPersistencia.Get(id);
        }

        public Tienda GetByFanpageId(string fanpageId)
        {
            return _tiendaPersistencia.GetByFanpageId(fanpageId);
        }

        public Tienda Guardar(Tienda tienda)
        {
            Validar(tienda);

            return _tiendaPersistencia.Guardar(tienda);
        }

        public Tienda GetTienda(Guid apiKey, string email)
        {
            var token = GetAccessToken(apiKey, email);

            if (!token.IsOk)
                throw new ValidationException(token.Message);

            return _tiendaPersistencia.GetByEmailAndApiKey(email, apiKey);
        }

        public IList<Merchant> GetMerchants(Guid apiKey, string email)
        {
            sdk = new TotalCoin(email, apiKey);
            var apiResponse = sdk.GetMerchants();

            if (!apiResponse.IsOk)
                throw new ValidationException(apiResponse.Message);

            List<Merchant> returnList = new List<Merchant>();

            foreach (var merchant in apiResponse.Response)
            {
                returnList.Add(new Merchant() { Id = merchant.Id, Name = merchant.Name });
            }

            return returnList;
        }

        public ApiResponse<AuthorizeResponse> GetAccessToken(Guid apiKey, string email)
        {
            sdk = new TotalCoin(email, apiKey);
            var token = sdk.GetAccessToken();
            return token;
        }

        public string PerformCheckout(PurchaseInformation purchaseInformation)
        {
            if (sdk == null)
                throw new ValidationException("No se inicializo la libreria de TotalCoin");

            purchaseInformation.OperationChannel = "TIENDATC";

            var apiResponse = sdk.PerformCheckout(purchaseInformation);

            if (!apiResponse.IsOk)
                throw new ValidationException(apiResponse.Message);

            return apiResponse.Response.URL;
        }

        public void WarmUpEF() {
            _tiendaPersistencia.WarmUpEF();
        }

        #region Metodos privados

        private void Validar(Tienda tienda)
        {
            if (string.IsNullOrEmpty(tienda.Name))
                throw new ValidationException("El nombre de la tienda es un campo requerido.");

            if (string.IsNullOrEmpty(tienda.Image))
                throw new ValidationException("La imagen de la tienda es un campo requerido.");
        }

        #endregion Metodos privados
    }
}