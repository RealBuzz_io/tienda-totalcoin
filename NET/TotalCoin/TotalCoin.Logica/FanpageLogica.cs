﻿using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using TotalCoin.Entidades;
using TotalCoin.Entidades.Facebook;
using TotalCoin.ILogica;
using TotalCoin.IPersistencia;
using TotalCoin.Persistencia;

namespace TotalCoin.Logica
{
    public class FanpageLogica : IFanpageLogica
    {
        private ITiendaLogica _tiendaLogica;
        private IFanpagePersistencia _persistencia;

        #region Constructores

        public FanpageLogica()
            : this(new TiendaLogica(), new FanpagePersistencia())
        {
        }

        public FanpageLogica(ITiendaLogica tiendaLogica, IFanpagePersistencia persistencia)
        {
            _tiendaLogica = tiendaLogica;
            _persistencia = persistencia;
        }

        #endregion Constructores

        public void Guardar(IList<Fanpage> fanpages, string appId, int tiendaId)
        {
            if (fanpages != null)
            {
                var tienda = _tiendaLogica.GetById(tiendaId);
                foreach (var fanpage in fanpages.Where(f => f.Integrada).ToList())
                {
                    if (!string.IsNullOrEmpty(fanpage.PageToken))
                        AgregarTab(fanpage, appId, tienda);
                }

                foreach (var fanpage in fanpages.Where(f => !f.Integrada).ToList())
                {
                    if (!string.IsNullOrEmpty(fanpage.PageToken))
                        RemoverTab(fanpage, appId);
                }
            }
        }

        public IList<Fanpage> GetByAccessToken(string token, string appId)
        {
            var client = new FacebookClient(token);

            var response = client.Get<FacebookFanpagesResponse>("me/accounts", new { fields = "name, access_token", limit = 30 });

            foreach (var fanpage in response.Fanpages)
            {
                fanpage.Integrada = FanpageIntegrada(fanpage, appId);
            }

            return response.Fanpages;
        }

        public void ActualizarFanpages(string token, string appId, int tiendaId)
        {
            var fanpages = GetByAccessToken(token, appId);

            var tienda = _tiendaLogica.GetById(tiendaId);

            foreach (var fp in fanpages.Where(f => f.Integrada).ToList())
                SetearPropiedades(fp, appId, tienda);
        }

        #region Metodos privados

        private bool FanpageIntegrada(Fanpage fanpage, string appId)
        {
            var client = new FacebookClient(fanpage.PageToken);
            var response = client.Get<FacebookTabsResponse>(string.Format("{0}/tabs", fanpage.Id), new { fields = "name, application", limit = 30 });

            return response.Tabs.Any(t => t.Application != null && t.Application.Id == appId);
        }

        private void AgregarTab(Fanpage fanpage, string appId, Tienda tienda)
        {
            try
            {
                var client = new FacebookClient(fanpage.PageToken);
                var response = client.Post(string.Format("{0}/tabs", fanpage.Id), new { app_id = appId });

                _persistencia.AgregarFanpage(fanpage, tienda);

                SetearPropiedades(client, fanpage, appId, tienda);
            }
            catch (Exception)
            {
                return;
            }
        }

        private void SetearPropiedades(FacebookClient client, Fanpage fanpage, string appId, Tienda tienda)
        {
            client.Post(string.Format("{0}/tabs/app_{1}", fanpage.Id, appId), new { custom_name = tienda.Name, custom_image_url = tienda.Image, position = 1 });
        }

        private void RemoverTab(Fanpage fanpage, string appId)
        {
            try
            {
                var client = new FacebookClient(fanpage.PageToken);
                var response = client.Delete(string.Format("{0}/tabs/app_{1}", fanpage.Id, appId));

                _persistencia.EliminarFanpage(fanpage);
            }
            catch (Exception)
            {
                //La tab nunca estuvo vinculada
                return;
            }
        }

        private void SetearPropiedades(Fanpage fanpage, string appId, Tienda tienda)
        {
            var client = new FacebookClient(fanpage.PageToken);
            SetearPropiedades(client, fanpage, appId, tienda);
        }

        #endregion Metodos privados
    }
}