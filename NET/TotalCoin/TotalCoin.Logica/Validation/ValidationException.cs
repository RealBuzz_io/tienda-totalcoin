﻿using System;

namespace TotalCoin.Logica
{
    public class ValidationException : Exception
    {
        public ValidationException(string message)
            : base(message)
        {
        }
    }
}