﻿using System.Collections.Generic;
using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.IPersistencia;
using TotalCoin.Persistencia;

namespace TotalCoin.Logica
{
    public class CategoriaLogica : ICategoriaLogica
    {
        private ICategoriaPersistencia _persistencia;

        #region Constructores

        public CategoriaLogica(ICategoriaPersistencia persistencia)
        {
            _persistencia = persistencia;
        }

        public CategoriaLogica()
            : this(new CategoriaPersistencia())
        {
        }

        #endregion Constructores

        public IList<Categoria> GetAllCategorias()
        {
            return _persistencia.GetAllCategoria();
        }

        public IList<Categoria> GetAllSubcategorias(int idPadre)
        {
            return _persistencia.GetAllSubcategoria(idPadre);
        }

        public Categoria GetById(int id)
        {
            return _persistencia.GetById(id);
        }
    }
}