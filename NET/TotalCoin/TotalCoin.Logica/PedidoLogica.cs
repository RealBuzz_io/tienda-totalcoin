﻿using TotalCoin.Entidades;
using TotalCoin.ILogica;
using TotalCoin.IPersistencia;
using TotalCoin.Persistencia;
using TotalCoin.Entidades.Enums;

namespace TotalCoin.Logica
{
    public class PedidoLogica : IPedidoLogica
    {
        private IPedidoPersistencia _persistenciaPedido;
        private IProductoPersistencia _persistenciaProducto;

        #region Constructores

        public PedidoLogica(IPedidoPersistencia persistenciaPedido, IProductoPersistencia persistenciaProducto)
        {
            _persistenciaPedido = persistenciaPedido;
            _persistenciaProducto = persistenciaProducto;
        }

        public PedidoLogica()
            : this(new PedidoPersistencia(), new ProductoPersistencia())
        {
        }

        #endregion Constructores

        public int Nuevo(Producto producto, int cantidad)
        {
            var pedido = new Pedido(producto, cantidad, Estado.Pendiente);
            return _persistenciaPedido.Guardar(pedido);
        }

        public Pedido GetById(int id)
        {
            return _persistenciaPedido.GetById(id);
        }

        public bool Confirmar(int id)
        {
            var pedido = _persistenciaPedido.GetById(id);
            if (_persistenciaPedido.Confirmar(id))
            {
                //Si estaba confirmado, le vuelvo a sumar el stock
                if (pedido.Status == (int)Estado.Pendiente || pedido.Status == (int)Estado.Cancelado)
                {
                    var producto = _persistenciaProducto.GetById(pedido.ProductId);

                    producto.Stock -= pedido.Amount;
                    _persistenciaProducto.Update(producto);
                }
                return true;
            }

            return false;
        }

        public bool Cancelar(int id)
        {
            var pedido = _persistenciaPedido.GetById(id);
            if (_persistenciaPedido.Cancelar(id))
            {
                //Si estaba confirmado, le vuelvo a sumar el stock
                if (pedido.Status == (int)Estado.Confirmado)
                {
                    var producto = _persistenciaProducto.GetById(pedido.ProductId);

                    producto.Stock += pedido.Amount;
                    _persistenciaProducto.Update(producto);
                }

                return true;
            }

            return false;
        }
    }
}