﻿using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.IPersistencia
{
    public interface IProductoPersistencia
    {
        IList<Producto> GetByTiendaId(int tiendaId);

        IList<Producto> GetByTiendaId(int tiendaId, int start, int limit, string busqueda, int? categoriaId);

        void Eliminar(Producto producto);

        Producto Insert(Producto producto);

        Producto Update(Producto producto);

        Producto GetById(int id);

        int GetCantidadByTienda(int tiendaId, string busqueda, int? categoriaId);
    }
}