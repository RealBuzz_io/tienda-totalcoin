﻿using System;
using TotalCoin.Entidades;

namespace TotalCoin.IPersistencia
{
    public interface ITiendaPersistencia
    {
        Tienda Guardar(Tienda tienda);

        Tienda Get(int id);

        Tienda GetByEmailAndApiKey(string email, Guid apiKey);

        Tienda GetByFanpageId(string fanpageId);

        void WarmUpEF();
    }
}