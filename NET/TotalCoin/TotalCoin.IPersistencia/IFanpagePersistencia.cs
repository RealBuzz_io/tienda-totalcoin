﻿using TotalCoin.Entidades;
using TotalCoin.Entidades.Facebook;

namespace TotalCoin.IPersistencia
{
    public interface IFanpagePersistencia
    {
        void EliminarFanpage(Fanpage fanpage);

        void AgregarFanpage(Fanpage fanpage, Tienda tienda);
    }
}