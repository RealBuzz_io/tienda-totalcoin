﻿using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.IPersistencia
{
    public interface ICategoriaPersistencia
    {
        IList<Categoria> GetAllCategoria();

        IList<Categoria> GetAllSubcategoria(int idPadre);

        Categoria GetById(int id);
    }
}