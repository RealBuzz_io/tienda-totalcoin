﻿using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.IPersistencia
{
    public interface IFormaDePagoPersistencia
    {
        FormaDePago GetById(int id);

        IList<FormaDePago> GetAll();
    }
}