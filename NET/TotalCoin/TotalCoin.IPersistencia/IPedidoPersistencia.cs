﻿using TotalCoin.Entidades;

namespace TotalCoin.IPersistencia
{
    public interface IPedidoPersistencia
    {
        int Guardar(Pedido pedido);

        Pedido GetById(int id);

        bool Confirmar(int id);

        bool Cancelar(int id);
    }
}