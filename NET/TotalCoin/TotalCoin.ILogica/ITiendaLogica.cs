﻿using System;
using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.ILogica
{
    public interface ITiendaLogica
    {
        Tienda GetById(int id);

        Tienda GetByFanpageId(string fanpageId);

        Tienda Guardar(Tienda tienda);

        Tienda GetTienda(Guid apiKey, string email);

        ApiResponse<AuthorizeResponse> GetAccessToken(Guid apiKey, string email);

        string PerformCheckout(PurchaseInformation purchaseInformation);

        IList<Merchant> GetMerchants(Guid apiKey, string email);

        void WarmUpEF();
    }
}