﻿using System.Collections.Generic;
using TotalCoin.Entidades.Facebook;

namespace TotalCoin.ILogica
{
    public interface IFanpageLogica
    {
        void Guardar(IList<Fanpage> fanpage, string appId, int tiendaId);

        IList<Fanpage> GetByAccessToken(string token, string appId);

        void ActualizarFanpages(string token, string appId, int tiendaId);
    }
}