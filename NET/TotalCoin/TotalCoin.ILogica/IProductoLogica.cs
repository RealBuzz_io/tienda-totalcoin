﻿using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.ILogica
{
    public interface IProductoLogica
    {
        IList<Producto> GetByTienda(int tiendaId);

        Producto Guardar(Producto producto);

        Producto GetById(int id);

        void Eliminar(int id);

        string GenerarPedido(Producto producto, int cantidad);

        IList<Producto> GetPaginado(int tiendaId, int pagina, int cantProductos, string busqueda, int? categoriaId);

        IList<Producto> GetPaginado(int tiendaId, int pagina, int cantProductos);

        int GetCantidadDePaginas(int tiendaId, int cantProductos);

        int GetCantidadDePaginas(int tiendaId, int cantProductos, string busqueda, int? categoriaId);
    }
}