﻿using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.ILogica
{
    public interface IFormaDePagoLogica
    {
        FormaDePago GetById(int id);

        IList<FormaDePago> GetAll();
    }
}