﻿using TotalCoin.Entidades;

namespace TotalCoin.ILogica
{
    public interface IPedidoLogica
    {
        int Nuevo(Producto producto, int cantidad);

        Pedido GetById(int id);

        bool Confirmar(int id);
        
        bool Cancelar(int id);
    }
}