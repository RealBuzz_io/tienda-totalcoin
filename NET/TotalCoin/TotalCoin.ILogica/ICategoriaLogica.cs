﻿using System.Collections.Generic;
using TotalCoin.Entidades;

namespace TotalCoin.ILogica
{
    public interface ICategoriaLogica
    {
        IList<Categoria> GetAllCategorias();

        IList<Categoria> GetAllSubcategorias(int idPadre);

        Categoria GetById(int p);
    }
}