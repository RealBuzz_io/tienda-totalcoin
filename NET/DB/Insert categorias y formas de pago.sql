﻿INSERT INTO PaymentMethod(Name) VALUES ('CREDITCARD')
INSERT INTO PaymentMethod(Name) VALUES ('CASH')
INSERT INTO PaymentMethod(Name) VALUES ('TOTALCOIN')

---------

DECLARE @CategoryId int

INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Accesorios para Vehículos')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Acc. para Motos y Cuatriciclos'),
(@CategoryId, 'Accesorios Náuticos'),
(@CategoryId, 'Accesorios para Autos'),
(@CategoryId, 'Accesorios para Camiones'),
(@CategoryId, 'Accesorios para Camionetas'),
(@CategoryId, 'Audio'),
(@CategoryId, 'GNC'),
(@CategoryId, 'Herramientas'),
(@CategoryId, 'Limpieza de Vehículos'),
(@CategoryId, 'Navegadores GPS para Vehículos'),
(@CategoryId, 'Neumáticos, Llantas y Tazas'),
(@CategoryId, 'Seguridad Vehícular'),
(@CategoryId, 'Tuning')

-----------------
GO

DECLARE @CategoryId int

INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Animales y Mascotas')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Aves'),
(@CategoryId, 'Caballos'),
(@CategoryId, 'Conejos'),
(@CategoryId, 'Gatos'),
(@CategoryId, 'Libros de Animales'),
(@CategoryId, 'Peces'),
(@CategoryId, 'Perros'),
(@CategoryId, 'Reptiles y Anfibios'),
(@CategoryId, 'Roedores')

--------------------------------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Antigüedades ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Artículos Marítimos Antiguos '),
(@CategoryId, 'Audio Antiguo '),
(@CategoryId, 'Balanzas Antiguas'),
(@CategoryId, 'Carteles Antiguos '),
(@CategoryId, 'Cristalería Antigua'),
(@CategoryId, 'Cámaras Fotográficas Antiguas '),
(@CategoryId, 'Decoración Antigua'),
(@CategoryId, 'Electrodomésticos Antiguos '),
(@CategoryId, 'Equipos Científicos Antiguos'),
(@CategoryId, 'Herramientas Antiguas'),
(@CategoryId, 'Iluminación Antigua '),
(@CategoryId, 'Indumentaria Antigua'),
(@CategoryId, 'Instrumentos Musicales  '),
(@CategoryId, 'Joyas y Relojes Antiguos '),
(@CategoryId, 'Juguetes Antiguos'),
(@CategoryId, 'Libros Antiguos'),
(@CategoryId, 'Llaves y Candados Antiguos '),
(@CategoryId, 'Muebles Antiguos'),
(@CategoryId, 'Máquinas de Escribir Antiguas '),
(@CategoryId, 'Platería Antigua '),
(@CategoryId, 'Registradoras Antiguas '),
(@CategoryId, 'Rejas y Portones Antiguos'),
(@CategoryId, 'Ropa de Cama Antigua'),
(@CategoryId, 'Sifones Antiguos '),
(@CategoryId, 'Sulkys y Carros Antiguos'),
(@CategoryId, 'Teléfonos Antiguos '),
(@CategoryId, 'Vajilla Antigua '),
(@CategoryId, 'Valijas Antiguas ')

------------------------------------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Arte y Artesanías')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Arte '),
(@CategoryId, 'Artesanías '),
(@CategoryId, 'Esculturas'),
(@CategoryId, 'Libros '),
(@CategoryId, 'Materiales para Tatuajes '),
(@CategoryId, 'Souvenirs')

---------------------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Autos, Motos y Otros ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Autos de Colección'),
(@CategoryId, 'Autos y Camionetas'),
(@CategoryId, 'Camiones '),
(@CategoryId, 'Motos '),
(@CategoryId, 'Náutica'),
(@CategoryId, 'Planes de Ahorro ')

------------------------------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Bebés ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Alimentación para Bebés '),
(@CategoryId, 'Andadores y Caminadores '),
(@CategoryId, 'Arneses para Autos '),
(@CategoryId, 'Artículos de Bebés para Baño'),
(@CategoryId, 'Artículos para Embarazadas '),
(@CategoryId, 'Artículos para la Salud '),
(@CategoryId, 'Cochecitos para Bebés'),
(@CategoryId, 'Corralitos '),
(@CategoryId, 'Cuarto de Bebé'),
(@CategoryId, 'Huevitos y Sillitas para Autos'),
(@CategoryId, 'Juguetes para Bebés '),
(@CategoryId, 'Pañales y Pañaleras'),
(@CategoryId, 'Recuerdos'),
(@CategoryId, 'Ropa para Bebés '),
(@CategoryId, 'Seguridad para Bebés'),
(@CategoryId, 'Sillas de Comer '),
(@CategoryId, 'Triciclos y Kartings')

-------------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Celulares y Teléfonos ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Accesorios para Celulares '),
(@CategoryId, 'Celulares y Smartphones '),
(@CategoryId, 'Centrales Telefónicas'),
(@CategoryId, 'Fax '),
(@CategoryId, 'Handies '),
(@CategoryId, 'Nextel'),
(@CategoryId, 'Radiofrecuencia'),
(@CategoryId, 'Repuestos de Celulares '),
(@CategoryId, 'Tarifadores y Cabinas'),
(@CategoryId, 'Telefonía Fija e Inalámbrica'),
(@CategoryId, 'Telefonía IP')

----------------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Coleccionables y Hobbies ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Cartas - R.P.G'),
(@CategoryId, 'Cigarrillos y Afines '),
(@CategoryId, 'Coleccionables de Coca-Cola '),
(@CategoryId, 'Colecciones Diversas '),
(@CategoryId, 'Comics '),
(@CategoryId, 'Figuras de Acción '),
(@CategoryId, 'Figuritas, Álbumes y Cromos '),
(@CategoryId, 'Filatelia '),
(@CategoryId, 'Lapiceras, Plumas y Bolígrafos '),
(@CategoryId, 'Latas, Botellas y Afines '),
(@CategoryId, 'Militaría y Afines'),
(@CategoryId, 'Modelismo '),
(@CategoryId, 'Monedas y Billetes '),
(@CategoryId, 'Muñecos'),
(@CategoryId, 'Posters, Carteles y Fotos '),
(@CategoryId, 'Posters, Carteles y Fotos '),
(@CategoryId, 'Vehículos en Miniatura ')

---------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Computación ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'All In One '),
(@CategoryId, 'Apple'),
(@CategoryId, 'CDs y DVDs Vírgenes '),
(@CategoryId, 'Componentes de PC'),
(@CategoryId, 'Fuentes, UPS y Estabilizadores'),
(@CategoryId, 'Impresoras y Accesorios'),
(@CategoryId, 'Lectores y Scanners '),
(@CategoryId, 'Memorias RAM '),
(@CategoryId, 'Monitores '),
(@CategoryId, 'Netbooks y Accesorios '),
(@CategoryId, 'Notebooks y Accesorios '),
(@CategoryId, 'PC '),
(@CategoryId, 'Palms y Handhelds '),
(@CategoryId, 'Pen Drives'),
(@CategoryId, 'Periféricos de PC '),
(@CategoryId, 'Procesadores'),
(@CategoryId, 'Proyectores y Pantallas '),
(@CategoryId, 'Redes'),
(@CategoryId, 'Software '),
(@CategoryId, 'Ultrabooks  '),
(@CategoryId, 'iPad y Tablets ')

---------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Consolas y Videojuegos')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Disney Infinity '),
(@CategoryId, 'Flippers y Arcade '),
(@CategoryId, 'Game Boy Advance '),
(@CategoryId, 'GameCube '),
(@CategoryId, 'Juegos y Joysticks para PC '),
(@CategoryId, 'Nintendo 3DS '),
(@CategoryId, 'Nintendo Ds '),
(@CategoryId, 'Nintendo Wii'),
(@CategoryId, 'Nintendo Wii U '),
(@CategoryId, 'PlayStation Portable - PSP '),
(@CategoryId, 'PlayStation 1 - PSX'),
(@CategoryId, 'PlayStation 2 - PS2'),
(@CategoryId, 'PlayStation 3 - PS3 '),
(@CategoryId, 'PlayStation 4 - PS4 '),
(@CategoryId, 'PlayStation Vita - PS Vita'),
(@CategoryId, 'Xbox '),
(@CategoryId, 'Xbox 360'),
(@CategoryId, 'Xbox One')

----------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Cámaras y Accesorios ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Accesorios para Cámaras '),
(@CategoryId, 'Baterías, Pilas y Cargadores '),
(@CategoryId, 'Cámaras Analógicas y Polaroid '),
(@CategoryId, 'Cámaras Digitales'),
(@CategoryId, 'Laboratorios y Mini Labs '),
(@CategoryId, 'Memorias '),
(@CategoryId, 'Portarretratos y Álbumes'),
(@CategoryId, 'Réflex y Lentes'),
(@CategoryId, 'Telescopios y Binoculares '),
(@CategoryId, 'Videocámaras Analógicas '),
(@CategoryId, 'Videocámaras Digitales ')

-------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Delicatessen y Vinos')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Bebidas Blancas '),
(@CategoryId, 'Bebidas Energizantes'),
(@CategoryId, 'Champagne '),
(@CategoryId, 'Cigarros y Pipas'),
(@CategoryId, 'Fernets'),
(@CategoryId, 'Libros de Cocina'),
(@CategoryId, 'Licores'),
(@CategoryId, 'Vinos '),
(@CategoryId, 'Whiskies ')
------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Deportes y Fitness')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Aerobics y Fitness '),
(@CategoryId, 'Artes Marciales y Boxeo '),
(@CategoryId, 'Bicicletas y Ciclismo '),
(@CategoryId, 'Básquet '),
(@CategoryId, 'Camping '),
(@CategoryId, 'Deportes Acuáticos'),
(@CategoryId, 'Deportes Extremos'),
(@CategoryId, 'Entradas a Eventos Deportivos '),
(@CategoryId, 'Equitación y Polo'),
(@CategoryId, 'Fútbol '),
(@CategoryId, 'Fútbol Americano '),
(@CategoryId, 'Golf '),
(@CategoryId, 'Hándbol'),
(@CategoryId, 'Hockey'),
(@CategoryId, 'Juegos de Salón'),
(@CategoryId, 'Libros y Revistas de Deportes '),
(@CategoryId, 'Montañismo y Tracking '),
(@CategoryId, 'Natación '),
(@CategoryId, 'Patín, Gimnasia y Danza '),
(@CategoryId, 'Pesca'),
(@CategoryId, 'Pilates y Yoga'),
(@CategoryId, 'Pulsímetros y Cronómetros '),
(@CategoryId, 'Rugby '),
(@CategoryId, 'Ski y Snowboard '),
(@CategoryId, 'Softball y Béisbol '),
(@CategoryId, 'Suplementos Alimenticios '),
(@CategoryId, 'Tenis, Pádel y Squash '),
(@CategoryId, 'Voley'),
(@CategoryId, 'Zapatillas ')

-------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Electrodomésticos y Aires Ac. ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Aires Acondicionados '),
(@CategoryId, 'Artefactos de Cuidado Personal '),
(@CategoryId, 'Artefactos para el Hogar '),
(@CategoryId, 'Calefacción'),
(@CategoryId, 'Cocción'),
(@CategoryId, 'Dispensadores y Purificadores'),
(@CategoryId, 'Electrodomésticos de Cocina'),
(@CategoryId, 'Heladeras y Freezers'),
(@CategoryId, 'Lavarropas y Secarropas '),
(@CategoryId, 'TVs, LCDs, LEDs y Plasmas ')
---------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Electrónica, Audio y Video ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Accesorios para Audio y Video'),
(@CategoryId, 'Audio / Video Profesional y DJ'),
(@CategoryId, 'Audio Portátil y Radios'),
(@CategoryId, 'Audio para Vehículos'),
(@CategoryId, 'Audio para el Hogar '),
(@CategoryId, 'Audio para el Hogar '),
(@CategoryId, 'Componentes Electrónicos'),
(@CategoryId, 'Fotocopiadoras y Accesorios'),
(@CategoryId, 'GPS '),
(@CategoryId, 'Home Theaters'),
(@CategoryId, 'MP3, MP4 y MP5 Players '),
(@CategoryId, 'Pilas, Cargadores y Baterías'),
(@CategoryId, 'Portarretratos Digitales'),
(@CategoryId, 'Proyectores y Pantallas'),
(@CategoryId, 'Reproductores de DVD y Video'),
(@CategoryId, 'Seguridad y Vigilancia - Hogar'),
(@CategoryId, 'Soportes '),
(@CategoryId, 'TVs, LCDs, LEDs y Plasmas'),
(@CategoryId, 'Video Cámaras Analógicas '),
(@CategoryId, 'Video Cámaras Digitales'),
(@CategoryId, 'iPad y Tablets'),
(@CategoryId, 'iPod ')

-----------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Entradas para Eventos ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Entradas de Colección '),
(@CategoryId, 'Eventos Deportivos '),
(@CategoryId, 'Eventos en Estados Unidos '),
(@CategoryId, 'Recitales '),
(@CategoryId, 'Teatro ')

----------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Hogar, Muebles y Jardín ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Baño '),
(@CategoryId, 'Cocina'),
(@CategoryId, 'Decoración para el Hogar'),
(@CategoryId, 'Dormitorio '),
(@CategoryId, 'Decoración para el Hogar'),
(@CategoryId, 'Dormitorio '),
(@CategoryId, 'Iluminación para el Hogar '),
(@CategoryId, 'Jardines y Exteriores'),
(@CategoryId, 'Muebles para Oficinas'),
(@CategoryId, 'Navidad'),
(@CategoryId, 'Pisos, Paredes y Aberturas'),
(@CategoryId, 'Productos de Limpieza'),
(@CategoryId, 'Sala de Estar y Comedor '),
(@CategoryId, 'Seguridad para el Hogar ')
------------

GO
DECLARE @CategoryId int

INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Industrias y Oficinas ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Agro '),
(@CategoryId, 'Balanzas '),
(@CategoryId, 'Componentes Eléctricos'),
(@CategoryId, 'Construcción '),
(@CategoryId, 'Embalajes '),
(@CategoryId, 'Equipamiento Comercial'),
(@CategoryId, 'Equipamiento Médico'),
(@CategoryId, 'Equipamiento para Oficinas '),
(@CategoryId, 'Herramientas '),
(@CategoryId, 'Industria Gastronómica '),
(@CategoryId, 'Industria Textil'),
(@CategoryId, 'Material de Promoción'),
(@CategoryId, 'Para Arquitectura y Diseño '),
(@CategoryId, 'Seguridad Industrial '),
(@CategoryId, 'Uniformes ')

----------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Inmuebles ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Campos '),
(@CategoryId, 'Casas '),
(@CategoryId, 'Cocheras '),
(@CategoryId, 'Departamentos'),
(@CategoryId, 'Depósitos y Galpones '),
(@CategoryId, 'Fondo de Comercio '),
(@CategoryId, 'Locales '),
(@CategoryId, 'Oficinas y Consultorios '),
(@CategoryId, 'PH'),
(@CategoryId, 'Parcelas, Nichos y Bóvedas'),
(@CategoryId, 'Quintas'),
(@CategoryId, 'Terrenos y Lotes '),
(@CategoryId, 'Tiempo Compartido ')
-----------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Instrumentos Musicales')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Amplificadores '),
(@CategoryId, 'Bajos '),
(@CategoryId, 'Baterías y Percusión '),
(@CategoryId, 'Consolas de Sonido'),
(@CategoryId, 'Efectos de Sonido'),
(@CategoryId, 'Guitarras'),
(@CategoryId, 'Instrumentos de Cuerdas'),
(@CategoryId, 'Instrumentos de Viento'),
(@CategoryId, 'Micrófonos, Pies y Preamp.'),
(@CategoryId, 'Parlantes'),
(@CategoryId, 'Partituras y Letras '),
(@CategoryId, 'Teclados y Pianos ')
--------------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Joyas y Relojes ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Bijouterie de Fantasía '),
(@CategoryId, 'Joyas'),
(@CategoryId, 'Joyas Antiguas '),
(@CategoryId, 'Materiales para Joyería'),
(@CategoryId, 'Pulsómetros y Cronómetros'),
(@CategoryId, 'Relojes Antiguos'),
(@CategoryId, 'Relojes Pulsera '),
(@CategoryId, 'Relojes para el Hogar ')

-------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Juegos y Juguetes ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Armas de Juguete '),
(@CategoryId, 'Autos de Juguete'),
(@CategoryId, 'Cartas y Naipes '),
(@CategoryId, 'Disfraces y Cotillón '),
(@CategoryId, 'Juegos'),
(@CategoryId, 'Juegos de Aire Libre y Agua'),
(@CategoryId, 'Juguetes '),
(@CategoryId, 'Modelismo'),
(@CategoryId, 'Muñecas'),
(@CategoryId, 'Muñecos y Accesorios '),
(@CategoryId, 'Peloteros y Castillos'),
(@CategoryId, 'Películas Infantiles '),
(@CategoryId, 'Soldados de Plomo'),
(@CategoryId, 'Vehículos en Miniatura')

-----------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Libros, Revistas y Comics ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Agendas y Diarios Íntimos'),
(@CategoryId, 'Biografías '),
(@CategoryId, 'Comics e Historietas '),
(@CategoryId, 'Diccionarios y Enciclopedias '),
(@CategoryId, 'Ensayos '),
(@CategoryId, 'Libros Antiguos'),
(@CategoryId, 'Libros Esotéricos/Paranormales'),
(@CategoryId, 'Libros Técnicos'),
(@CategoryId, 'Libros de Arquitectura y Diseño '),
(@CategoryId, 'Libros de Arte '),
(@CategoryId, 'Libros de Autoayuda '),
(@CategoryId, 'Libros de Ciencias Económicas '),
(@CategoryId, 'Libros de Ciencias Sociales '),
(@CategoryId, 'Libros de Computación/Internet '),
(@CategoryId, 'Libros de Cs Humanísticas'),
(@CategoryId, 'Libros de Cs Médicas/Naturales'),
(@CategoryId, 'Libros de Derecho'),
(@CategoryId, 'Libros de Ficción '),
(@CategoryId, 'Libros de Idiomas '),
(@CategoryId, 'Libros de Ingeniería '),
(@CategoryId, 'Libros de Recreación y Hobbies '),
(@CategoryId, 'Libros de Religión '),
(@CategoryId, 'Libros de Texto y Escolares '),
(@CategoryId, 'Revistas ')

-------------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
(NULL, 'Música, Películas y Series ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Merchandising '),
(@CategoryId, 'Música '),
(@CategoryId, 'Películas'),
(@CategoryId, 'Series de TV')

-----------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Otras categorías ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId , 'Adultos '),
(@CategoryId , 'Artículos de Mercería '),
(@CategoryId , 'Esoterismo '),
(@CategoryId , 'Útiles Escolares ')

--------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Ropa y Accesorios')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Accesorios de Moda '),
(@CategoryId, 'Bermudas y Shorts '),
(@CategoryId, 'Buzos y Hoodies '),
(@CategoryId, 'Camisas, Chombas y Blusas '),
(@CategoryId, 'Camperas, Tapados y Trenchs'),
(@CategoryId, 'Carteras, Bolsos y Billeteras '),
(@CategoryId, 'Enteritos'),
(@CategoryId, 'Lentes '),
(@CategoryId, 'Pantalones, Jeans y Calzas '),
(@CategoryId, 'Remeras y Musculosas'),
(@CategoryId, 'Ropa Deportiva'),
(@CategoryId, 'Ropa Interior y de Dormir'),
(@CategoryId, 'Ropa de Bebés'),
(@CategoryId, 'Saquitos, Sweaters y Chalecos'),
(@CategoryId, 'Trajes '),
(@CategoryId, 'Trajes de Baño'),
(@CategoryId, 'Uniformes '),
(@CategoryId, 'Vestidos y Polleras '),
(@CategoryId, 'Zapatillas '),
(@CategoryId, 'Zapatos y Sandalias ')
------------

GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Salud y Belleza ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES
(@CategoryId, 'Cuidado Bucal '),
(@CategoryId, 'Cuidado de la Piel '),
(@CategoryId, 'Cuidado de la Salud '),
(@CategoryId, 'Cuidado del Cabello '),
(@CategoryId, 'Cuidado del Cuerpo'),
(@CategoryId, 'Cuidado para Manos'),
(@CategoryId, 'Equipamiento Médico'),
(@CategoryId, 'Maquillaje'),
(@CategoryId, 'Perfumes y Fragancias '),
(@CategoryId, 'Suplementos Alimenticios'),
(@CategoryId, 'Terapias Naturales '),
(@CategoryId, 'Vitaminas y Complementos')

-----------
GO
DECLARE @CategoryId int
INSERT INTO Category (ParentCategoryId, Name) VALUES
( NULL, 'Servicios ')

SELECT @CategoryId = SCOPE_IDENTITY()

INSERT INTO Category (ParentCategoryId, Name) VALUES

(@CategoryId, 'Arte, Música y Cine'),
(@CategoryId, 'Belleza'),
(@CategoryId, 'Cursos y Clases'),
(@CategoryId, 'Delivery '),
(@CategoryId, 'Fiestas y Eventos '),
(@CategoryId, 'Hogar'),
(@CategoryId, 'Imprenta '),
(@CategoryId, 'Mantenimiento de Vehículos'),
(@CategoryId, 'Medicina y Salud '),
(@CategoryId, 'Profesionales '),
(@CategoryId, 'Ropa y Moda '),
(@CategoryId, 'Servicio Técnico '),
(@CategoryId, 'Servicios para Mascotas'),
(@CategoryId, 'Servicios para Oficinas '),
(@CategoryId, 'Transporte ')
